<?php

if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    //$post= Painel::selecionar(`tb_site.galeria`,'id = ?',array($id));
    $post = MySql::conectar()->prepare("SELECT * FROM `tb_site.galeria` WHERE id = ?");
    $post->execute(array($id));
    $post = $post->fetch();

} else {
    Painel::alerta('erro', 'Você precisa passar o parametro ID!');
    die();
}

?>
<header class="nk-header nk-header-opaque">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top">
        <div class="container">
            <div class="nk-nav-table">

                <a href="inicio" class="nk-nav-logo">

                    <img src="<?php echo INCLUDE_PATH; ?>assets/images/fullblack.png" alt="" width="82">
                </a>


                <ul class="nk-nav nk-nav-right nk-nav-icons">


                    <li class="single-icon">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>


<!--
START: Fullscreen Navbar

Additional Classes:
    .nk-navbar-align-center
    .nk-navbar-align-right
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="inicio" class="nk-nav-logo">
                            <img src="<?php echo INCLUDE_PATH; ?>assets/images/logo.png" alt="" width="52">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">
                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                                </li>

                                <li class=" nk-drop-item">
                                <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank"><i
                                        class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank"><i
                                        class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank"><i
                                        class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Fullscreen Navbar -->

<div class="nk-main">

    <div class="container-fluid">
        <div class="nk-portfolio-single nk-portfolio-single-half">
            <div class="row">
                <div class="col-lg-6 push-lg-6">
                    <div class="nk-sidebar-sticky" data-offset-top="0">
                        <div class="nk-portfolio-info">
                            <h1 class="nk-portfolio-title"><?php echo $post['nome']; ?></h1>
                            <div class="nk-portfolio-text"><?php echo $post['conteudo']; ?>
                                <p>
                                </p>
                            </div>


                            <!--
                    <a href="#" title="Share page on Google Plus" data-share="google-plus">Google Plus</a>,
                    <a href="#" title="Share page on LinkedIn" data-share="linkedin">LinkedIn</a>,
                    <a href="#" title="Share page on Vkontakte" data-share="vk">Vkontakte</a>
                    -->
                            </td>
                            </tr>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 pull-lg-6">
                    <div class="nk-portfolio-images">

                        <?php
                        $video = MySql::conectar()->prepare("SELECT * FROM `tb_site.video` WHERE `video_id` = ?");
                        $video->execute(array($id));
                        foreach ($video as $key => $value) {
                            $str = $value['url'];
                            preg_match('/(v\/|\?v=)([^&?\/]{5,15})/',$str,$x); //rouba o conteúde de "v" e manda para a variável $x
                            echo '<iframe width="560" height="315" src="http://www.youtube.com/embed/'.end($x).'" frameborder="0" allowfullscreen></iframe>';
                        }

                        $infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_admin_imagem` WHERE `id_produto` = $id ");
                        $infoSite->execute();
                        $infoSite = $infoSite->fetchAll();
                        foreach ($infoSite as $key => $value) {
                            ?>
                            <img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $value['img']; ?>">
                            <?php
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- START: Pagination -->

    <div class="nk-pagination nk-pagination-center bg-gray-2">
        <div class="container">
            <?php
            $a = MySql::conectar()->prepare("SELECT * FROM `tb_site.galeria` WHERE id < $id ORDER BY id ASC LIMIT 1");
            $a->execute();
            $a = $a->fetch();
            ?>
            <a class="nk-pagination-prev" href="<?php echo INCLUDE_PATH ?>pagina-unica?id=<?php echo $a['id']; ?>">
                <span class="pe-7s-angle-left"></span>Ver o Anterior</a>
            <a class="nk-pagination-center" href="projetos">
                <span class="nk-icon-squares"></span>
            </a>
            <?php
            $sql = MySql::conectar()->prepare("SELECT * FROM `tb_site.galeria` WHERE id > $id ORDER BY id ASC LIMIT 1");
            $sql->execute();
            $sql = $sql->fetch();
            ?>
            <a class="nk-pagination-next" href="<?php echo INCLUDE_PATH ?>pagina-unica?id=<?php echo $sql['id']; ?>">Ver
                o próximo<span class="pe-7s-angle-right"></span> </a>

        </div>
    </div>

</div>

<!-- END: Pagination -->
</div>