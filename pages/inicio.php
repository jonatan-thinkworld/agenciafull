<?php
$infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_site.inicio`");
$infoSite->execute();
$infoSite = $infoSite->fetch();
?>
<header class="nk-header">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-autohide nk-navbar-transparent nk-navbar-white-text-on-top">
        <div class="container">
            <div class="nk-nav-table">

                <a href="inicio" class="nk-nav-logo">
                    <img src="<?php echo INCLUDE_PATH;?>assets/images/logo.png" alt="" width="82"
                         class="nk-nav-logo-onscroll">
                    <img src="<?php echo INCLUDE_PATH; ?>assets/images/fullblack.png" alt="" width="82">
                </a>


                <ul class="nk-nav nk-nav-right nk-nav-icons">


                    <li class="single-icon">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>


<!--
START: Fullscreen Navbar

Additional Classes:
    .nk-navbar-align-center
    .nk-navbar-align-right
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="inicio" class="nk-nav-logo">
                            <img src="<?php echo INCLUDE_PATH; ?>assets/images/logo.png" alt="" width="52">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                                </li>

                                <li class=" nk-drop-item">
                                <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                                </li>
                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank"><i
                                        class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank"><i
                                        class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank"><i
                                        class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Fullscreen Navbar -->
<div class="nk-main">

    <!-- START: Header Title -->
<!-- START: Header Title -->
<!--
Additional Classes:
.nk-header-title-sm
.nk-header-title-md
.nk-header-title-lg
.nk-header-title-xl
.nk-header-title-full
.nk-header-title-parallax
.nk-header-title-parallax-opacity
-->
<div class="nk-header-title nk-header-title-lg nk-header-title-parallax nk-header-title-parallax-opacity">
    <div class="bg-image">
        <div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['img']; ?>')"></div>
        <div class="bg-image-overlay" style="background-color: rgba(12, 12, 12, 0.6);"></div>
    </div>
    <div class="nk-header-table">
        <div class="nk-header-table-cell">
            <div class="container">


                <h1 class="nk-title text-white"><?php echo $infoSite['titulo_topo']; ?></h1>


                <div class="nk-gap-3"></div>

                <a class="nk-video-fullscreen-toggle" href="https://vimeo.com/265816030">
                            <span class="nk-video-icon">
                                <span>
                                    <span class="nk-play-icon"></span>
                                </span>
                            </span>
                </a>


            </div>
        </div>
    </div>

</div>

<!-- fim do cabeçalho-->

<!-- inicio sobre fulldesign-->
<div class="nk-box bg-white-3 text-white">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="nk-gap-2"></div>

                <div class="nk-box-3 text-center" style="color: #5f5f5f;">

                    <div class="nk-gap mnt-5"></div>

                    <p class="text-black"><?php echo $infoSite['texto']; ?></p>

                </div>

                <div class="nk-gap-2"></div>
            </div>
        </div>
    </div>
</div>
<!-- fim do sobre fulldesign -->

<!-- inicio portifolio -->
<div class="container-fluid">
    <div class="nk-portfolio-list nk-isotope nk-isotope-3-cols">
        <?php
        $query = "SELECT * FROM `tb_site.galeria` ";
        $sql = MySql::conectar()->prepare($query);
        $sql->execute();
        $noticias = $sql->fetchAll();
        foreach($noticias as $key=>$value){
            $sql = MySql::conectar()->prepare("SELECT * FROM `tb_site.catprojeto` WHERE id = ?");
            $sql->execute(array($value['categoria_id']));
            $categoriaNome = $sql->fetch();
            ?>

        <div class="nk-isotope-item" data-filter="Design">
            <div class="nk-portfolio-item nk-portfolio-item-square nk-portfolio-item-info-style-1">
                <a href="<?php echo INCLUDE_PATH?>pagina-unica?id=<?php echo $value['id'];?>" class="nk-portfolio-item-link"></a>
                <div class="nk-portfolio-item-image">
                    <div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $value['img']; ?>');"></div>
                </div>
                <div class="nk-portfolio-item-info nk-portfolio-item-info-center text-center bg-main">
                    <div>
                        <h2 class="portfolio-item-title h3"><?php echo $value['nome'];?></h2>
                        <div class="portfolio-item-category"><?php echo $categoriaNome['nome']; ?></div>
                    </div>
                </div>
            </div>
        </div>
<?php } ?>
        </div>
        </div>
        <!-- fim portifolio -->

        <!-- inicio -->
        <div class="bg-white text-center">
            <div class="nk-gap-5 mnb-12"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h2>Precisa de um Projeto?</h2>
                        <div class="nk-gap-1 mnt-15"></div>
                        <p>
                            Potencialize a experiência da sua marca.<br>
                            Atendemos você onde estiver. Deixe seu contato e nossa equipe ligará para você.
                        </p>
                        <div class="nk-gap mnt-9"></div>
                        <a href="contato" class="nk-btn nk-btn-long">Entrar em contato</a>
                    </div>
                </div>
            </div>
            <div class="nk-gap-5"></div>
        </div>
</div>
        <!-- fim entre em contato-->