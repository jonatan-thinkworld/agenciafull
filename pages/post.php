<?php

if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    //$post= Painel::selecionar(`tb_site.blog`,'id = ?',array($id));
    $post = MySql::conectar()->prepare("SELECT * FROM `tb_site.blog` WHERE id = ?");
    $post->execute(array($id));
    $post = $post->fetch();

} else {
    Painel::alerta('erro', 'Você precisa passar o parametro ID!');
    die();
}

?>
<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Agência Full</title>


    <link rel="icon" type="image/png" href="<?php INCLUDE_PATH?>assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php INCLUDE_PATH?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php INCLUDE_PATH?>assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="<?php INCLUDE_PATH?>assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="<?php INCLUDE_PATH?>assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="<?php INCLUDE_PATH?>assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="<?php INCLUDE_PATH?>assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="<?php INCLUDE_PATH?>assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="<?php INCLUDE_PATH?>assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="<?php INCLUDE_PATH?>assets/bower_components/jquery/dist/jquery.min.js"></script>



</head>


<body>





<header class="nk-header nk-header-opaque">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top">
        <div class="container">
            <div class="nk-nav-table">

                <a href="index.html" class="nk-nav-logo">

                    <img src="<?php INCLUDE_PATH?>assets/images/fullblack.png" alt="" width="80">
                </a>


                <ul class="nk-nav nk-nav-right hidden-md-down" data-nav-mobile="#nk-nav-mobile">

                    <li>
                        <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                    </li>

                    <li class=" nk-drop-item">
                    <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                    </li>

                    <li>
                        <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                    </li>

                    <li>
                        <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                    </li>

                    <li>
                        <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                    </li>

                    <li>
                        <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                    </li>

                </ul>

                <ul class="nk-nav nk-nav-right nk-nav-icons">

                    <li class="single-icon hidden-lg-up">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>


                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>




<!--
START: Navbar Mobile

Additional Classes:
    .nk-navbar-align-center
    .nk-navbar-align-right
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="inicio" class="nk-nav-logo">
                            <img src="<?php echo INCLUDE_PATH;?>assets/images/logo.png" alt="" width="70">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                                </li>

                                <li class=" nk-drop-item">
                                <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Navbar Mobile -->


<div class="nk-main bg-white">

    <!-- START: Header Title -->
    <!--
Additional Classes:
    .nk-header-title-sm
    .nk-header-title-md
    .nk-header-title-lg
    .nk-header-title-xl
    .nk-header-title-full
    .nk-header-title-parallax
    .nk-header-title-parallax-opacity
-->
    <div class="nk-header-title nk-header-title-lg nk-header-title-parallax nk-header-title-parallax-opacity">
        <div class="bg-image">

            <div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $post['capa'];?>"></div>


        </div>
        <div class="nk-header-table">
            <div class="nk-header-table-cell">
                <div class="container">




                </div>
            </div>
        </div>

    </div>

    <!-- END: Header Title -->




    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="nk-gap-4"></div>

                <!-- START: Post -->
                <div class="nk-blog-post nk-blog-post-single">
                    <h1><?php echo $post['titulo'];?></h1>

                    <div class="nk-post-meta">
                        <?php
                        $verifica_categoria = MySql::conectar()->prepare("SELECT * FROM `tb_site.categoria` WHERE id = ?");
                        $verifica_categoria->execute(array($post['categoria_id']));
                        $verifica_categoria = $verifica_categoria->fetch();
                        $quant= MySql::conectar()->prepare("SELECT * FROM `tb_site.comentarios` WHERE blog_id = ?");
                        $quant->execute(array($post['id']));
                        //$quant = $quant->fetchAll();
                        $count = $quant->rowCount();

                        ?>
                        <?php echo $post['date'];?> / <a href="#"><?php echo $verifica_categoria['nome']?></a> /<?php echo $count?> Commentário(s)
                    </div>

                    <!-- START: Post Text -->
                    <div class="nk-post-text">.
                        <?php echo $post['conteudo'];?>
                      </div>
                    <!-- END: Post Text -->

                    <!-- START: Post Share -->
                    <div class="nk-post-share">
                        <strong>Compartilhar</strong>
                        <a href="#" title="Share page on Facebook" data-share="facebook">Facebook</a>,
                        <a href="#" title="Share page on Twitter" data-share="twitter">Twitter</a>
                        <!--
                    <a href="#" title="Share page on Google Plus" data-share="google-plus">Google Plus</a>,
                    <a href="#" title="Share page on LinkedIn" data-share="linkedin">LinkedIn</a>,
                    <a href="#" title="Share page on Vkontakte" data-share="vk">Vkontakte</a>
                    -->
                    </div>
                    <!-- END: Post Share -->
                </div>
                <!-- END: Post -->

                <div class="nk-gap-3"></div>
            </div>
        </div>
    </div>

    <!-- START: Comments -->
    <div id="comments"></div>
    <div class="nk-comments">
        <div class="nk-gap-3"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h3 class="nk-title">Comentários Existente</h3>
                    <div class="nk-gap-1"></div>

                    <!-- START: Comment -->
                    <?php
                    $comentarios = MySql::conectar()->prepare("SELECT * FROM `tb_site.comentarios` WHERE blog_id = ?");

                    $comentarios->execute(array($post['id']));
                    $comentarios = $comentarios->fetchAll();

                    /*
                            tb_site.resposta_comentarios:
                            id | comentario_id | nome | comentario
                    */
                    ?>
                    <?php
                    foreach ($comentarios as $key => $value) {
                    ?>
                    <div class="nk-comment">

                        <div class="nk-comment-meta">
                            <div class="nk-comment-name"><?php echo $value['nome']; ?></a></div>
                            <div class="nk-comment-date"><?php echo $value['data'] ?></div>
                        </div>
                        <div class="nk-comment-text">
                            <p><?php echo $value['comentario'] ?></p>
                    </div>
                    <!-- END: Comment -->
                </div>
                    <?php } ?>
            </div>

        </div>
        <div class="nk-gap-3"></div>
    </div>
    <!-- END: Comments -->

    <!-- START: Reply -->
        <?php
        //Inserção dos comentários na database!
        if(isset($_POST['postar_comentario'])){
            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $comentario = $_POST['mensagem'];
            $noticia_id = $_POST['blog_id'];
            $arr = ['nome'=>$nome,'email'=>$email,'comentario'=>$comentario,'blog_id'=>$noticia_id,'data'=>date('Y-m-d'),'order_id'=>'0', 'nome_tabela'=>'tb_site.comentarios'];
            Painel::inserir($arr);
            echo '<script>alert("Comentário realizado com sucesso!")</script>';


        }
        ?>
    <div class="nk-reply">
        <div class="nk-gap-3"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h3 class="nk-title">Faça um comentário</h3>
                    <div class="nk-gap-1"></div>
                    <form method="post" class="nk-form">
                        <div class="row vertical-gap">
                            <div class="col-md-6">
                                <input type="text" class="form-control required" name="nome" placeholder="Seu Nome">
                            </div>
                            <div class="col-md-6">
                                <input type="email" class="form-control required" name="email" placeholder="Seu E-mail">
                            </div>
                        </div>
                        <div class="nk-gap-1"></div>
                        <textarea class="form-control required" name="mensagem" rows="8" placeholder="Seu comentário" aria-required="true"></textarea>
                        <input type="hidden" name="blog_id" value="<?php echo $post['id']; ?>">
                        <div class="nk-gap-1"></div>
                        <div class="nk-form-response-success"></div>
                        <div class="nk-form-response-error"></div>
                        <input type="submit" name="postar_comentario" class="nk-btn" value="Postar o comentário">
                    </form>
                </div>
            </div>
        </div>
        <div class="nk-gap-3"></div>
    </div>
    <!-- END: Reply -->

    <!-- START: Pagination -->
    <div class="nk-pagination nk-pagination-center bg-gray-2">
        <div class="container">
            <?php
            $a = MySql::conectar()->prepare("SELECT * FROM `tb_site.blog` WHERE id < $id ORDER BY id DESC LIMIT 1");
            $a->execute();
            $a = $a->fetch();
            ?>
            <a class="nk-pagination-prev" href="<?php echo INCLUDE_PATH?>post?id=<?php echo $a['id'];?>">
                <span class="pe-7s-angle-left"></span>Post Anterior</a>
            <a class="nk-pagination-center" href="blog">
                <span class="nk-icon-squares"></span>
            </a>
            <?php
            $sql = MySql::conectar()->prepare("SELECT * FROM `tb_site.blog` WHERE id > $id ORDER BY id ASC LIMIT 1");
            $sql->execute();
            $sql = $sql->fetch();
            ?>
            <a class="nk-pagination-next" href="<?php echo INCLUDE_PATH?>post?id=<?php echo $sql['id'];?>">Próximo Post <span class="pe-7s-angle-right"></span> </a>
        </div>
    </div>
    <!-- END: Pagination -->
