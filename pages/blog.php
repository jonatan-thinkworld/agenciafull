<?php
$url = explode('/', $_GET['url']);
if (!isset($url[2])) {
    $categoria = MySql::conectar()->prepare("SELECT * FROM `tb_site.categoria` WHERE slug = ?");
    $categoria->execute(array(@$url[1]));
    $categoria = $categoria->fetch();
}
?>
<header class="nk-header nk-header-opaque">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top">
        <div class="container">
            <div class="nk-nav-table">

                <a href="inicio" class="nk-nav-logo">

                    <img src="<?php echo INCLUDE_PATH; ?>assets/images/fullblack.png" alt="" width="82">
                </a>


                <ul class="nk-nav nk-nav-right hidden-md-down" data-nav-mobile="#nk-nav-mobile">

                    <li class=" nk-drop-item">
                        <a href="<?php echo INCLUDE_PATH; ?>inicio">Início</a>
                    </li>

                    <li class=" nk-drop-item">
                    <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                    </li>

                    <li class=" nk-drop-item">
                        <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                    </li>
                    <li class=" nk-drop-item">
                        <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                    </li>

                    <li class="nk-drop-item">
                        <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                    <li>
                        <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                    </li>

                </ul>

                <ul class="nk-nav nk-nav-right nk-nav-icons">

                    <li class="single-icon hidden-lg-up">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>


                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="inicio" class="nk-nav-logo">
                            <img src="<?php echo INCLUDE_PATH; ?>assets/images/logo.png" alt="" width="52">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                                </li>
                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank"><i
                                        class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank"><i
                                        class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank"><i
                                        class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- fim do menu -->


<div class="nk-main bg-white">

    <!-- START: Header Title -->
    <!--
Additional Classes:
    .nk-header-title-sm
    .nk-header-title-md
    .nk-header-title-lg
    .nk-header-title-xl
    .nk-header-title-full
    .nk-header-title-parallax
    .nk-header-title-parallax-opacity
-->
    <div class="nk-header-title nk-header-title-sm nk-header-title-parallax nk-header-title-parallax-opacity">
        <?php $slide = Painel::selecionar('tb_admin_blog');
        ?>
        <div class="bg-image">
            <div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $slide['capa']; ?>');"></div>
            <div class="bg-image-overlay" style="background-color: rgba(20, 20, 20, 0.6);"></div>
        </div>
        <div class="nk-header-table">
            <div class="nk-header-table-cell">
                <div class="container">


                    <div class="nk-gap"></div>
                    <div class="nk-header-text">
                        <div class="lead-sm text-white"><?php echo $slide['titulo'];?>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
    <?php
    $porPagina = 10;

    $query = "SELECT * FROM `tb_site.blog` ";
    if($categoria['nome'] != ''){
        $categoria['id'] = (int)$categoria['id'];
        $query.="WHERE categoria_id = $categoria[id]";
    }
    if(isset($_POST['parametro'])){
        if(strstr($query,'WHERE') !== false){
            $busca = $_POST['parametro'];
            $query.=" AND titulo LIKE '%$busca%'";
        }else{
            $busca = $_POST['parametro'];
            $query.=" WHERE titulo LIKE '%$busca%'";
        }
    }
    $query2 = "SELECT * FROM `tb_site.blog` ";
    if($categoria['nome'] != ''){
        $categoria['id'] = (int)$categoria['id'];
        $query2.="WHERE categoria_id = $categoria[id]";
    }
    if(isset($_POST['parametro'])){
        if(strstr($query2,'WHERE') !== false){
            $busca = $_POST['parametro'];
            $query2.=" AND titulo LIKE '%$busca%'";
        }else{
            $busca = $_POST['parametro'];
            $query2.=" WHERE titulo LIKE '%$busca%'";
        }
    }
    $totalPaginas = MySql::conectar()->prepare($query2);
    $totalPaginas->execute();
    $totalPaginas = ceil($totalPaginas->rowCount() / $porPagina);
    if(!isset($_POST['parametro'])){
        if(isset($_GET['pagina'])){
            $pagina = (int)$_GET['pagina'];
            if($pagina > $totalPaginas){
                $pagina = 1;
            }

            $queryPg = ($pagina - 1) * $porPagina;
            $query.=" ORDER BY order_id ASC LIMIT $queryPg,$porPagina";
        }else{
            $pagina = 1;
            $query.=" ORDER BY order_id ASC LIMIT 0,$porPagina";
        }
    }else{

        $query.=" ORDER BY order_id ASC";
    }
    $sql = MySql::conectar()->prepare($query);
    $sql->execute();
    $noticias = $sql->fetchAll();

    ?>

    <div class="container">
        <!-- START: Filter -->
        <ul class="nk-isotope-filter">
            <li class="active" data-filter="*">Todos</li>
            <?php
            $categorias = Painel::selecionarTudo('tb_site.categoria');
            foreach ($categorias as $key => $value) {
                ?>
                <li  data-filter="<?php echo $value['nome']; ?>"><?php echo $value['nome']; ?></li>
            <?php } ?>
        </ul>
        <!-- END: Filter -->

        <!-- START: Posts List -->

        <div class="nk-blog-isotope nk-isotope nk-isotope-gap nk-isotope-2-cols">
            <?php
            foreach($noticias as $key=>$value){
                $sql = MySql::conectar()->prepare("SELECT * FROM `tb_site.categoria` WHERE id = ?");
                $sql->execute(array($value['categoria_id']));
                $categoriaNome = $sql->fetch();
                ?>

            <!-- START: Post -->
            <div class="nk-isotope-item" data-filter="<?php echo $categoriaNome['nome'];?>">

                <div class="nk-blog-post">
                    <div class="nk-post-thumb">
                        <a href="<?php echo INCLUDE_PATH; ?>post?id=<?php echo $value['id']; ?>">
                           <img  src="<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $value['capa'];?>" class="nk-img-stretch">

                        </a>
                    </div>
                    <h2 class="nk-post-title h5"><a href="<?php echo INCLUDE_PATH; ?>post?id=<?php echo $value['id']; ?>"><?php echo $value['titulo']; ?></a></h2>

                    <div class="nk-post-meta">
                        <?php echo date('d/m/Y',strtotime($value['date'])) ?> / <a href="<?php echo INCLUDE_PATH; ?>post?id=<?php echo $value['categoria_id']; ?>"><?php echo $categoriaNome['nome'];?></a>
                    </div>

                    <div class="nk-post-text">
                        <p><?php echo substr(strip_tags($value['conteudo']),0,400).'...';?></p>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
            <!-- END: Post -->


        </div>
        <!-- END: Posts List -->

        <div class="nk-gap-5"></div>

    </div>

    <!-- START: Pagination -->
    <div class="nk-pagination nk-pagination-center bg-gray-2">
        <a href="#">Carregar mais posts</a>
    </div>
    <!-- END: Pagination -->
