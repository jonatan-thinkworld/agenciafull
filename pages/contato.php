<?php
$infoSite = MySql::conectar()->prepare("SELECT * FROM `contato`");
$infoSite->execute();
$infoSite = $infoSite->fetch();
?>
<header class="nk-header nk-header-opaque">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top">
        <div class="container">
            <div class="nk-nav-table">

                <a href="inicio" class="nk-nav-logo">

                    <img src="<?php echo INCLUDE_PATH;?>assets/images/fullblack.png" alt="" width="82">
                </a>


                <ul class="nk-nav nk-nav-right nk-nav-icons">


                    <li class="single-icon">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>


<!--
START: Fullscreen Navbar

Additional Classes:
    .nk-navbar-align-center
    .nk-navbar-align-right
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="inicio" class="nk-nav-logo">
                            <img src="<?php echo INCLUDE_PATH;?>assets/images/logo.png" alt="" width="52">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                                </li>

                                <li class=" nk-drop-item">
                                <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                                </li>
                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Fullscreen Navbar -->
    <div class="nk-main bg-white">

        <!-- START: Google Map -->
        <div id="google-map-contact" class="nk-gmaps nk-gmaps-lg"></div>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyD3cOutXVXqkhzknMHP9OEDCXe9ch9W8Z8&sensor=false"></script>
        <script>
        function initializeGmaps() {
            var LatLng = {
                lat: -22.4982011,
                lng: -41.920905
            };
            var mapCanvas = document.getElementById('google-map-contact');
            var mapOptions = {
                center: LatLng,
                scrollwheel: false,
                zoom: 14,
                backgroundColor: 'none',
                mapTypeId: 'nKStyle'
            };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                icon: 'assets/images/marker.png',
                title: 'Agência Full'
            });

            // style from https://snazzymaps.com/style/151/ultra-light-with-labels
            var styledMapType = new google.maps.StyledMapType([{
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e9e9e9"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 29
                }, {
                    "weight": 0.2
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 18
                }]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 21
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dedede"
                }, {
                    "lightness": 21
                }]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "saturation": 36
                }, {
                    "color": "#333333"
                }, {
                    "lightness": 40
                }]
            }, {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f2f2f2"
                }, {
                    "lightness": 19
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 17
                }, {
                    "weight": 1.2
                }]
            }], {
                name: 'nKStyle'
            });
            map.mapTypes.set('nKStyle', styledMapType);
        }
        if (typeof google !== 'undefined') {
            google.maps.event.addDomListener(window, 'load', initializeGmaps);
        }
        </script>
        <!-- END: Google Map -->

        <!-- START: Contact Info -->
        <div class="container">
            <div class="nk-gap-5 mnt-10"></div>
            <div class="row vertical-gap">
                <div class="col-lg-5">
                    <!-- START: Info -->
                    <h2 style="color: #000;"><?php echo $infoSite['titulo'];?></h2>
                    <div class="nk-gap mnt-3"></div>

                    <ul class="nk-contact-info">
                        <li style="color: #000;">
                            <?php echo $infoSite['contato'];?>
                            </li>
                        <li>

                    </ul>
                    <!-- END: Info -->
                </div>
                <div class="col-lg-7">
                    <!-- START: Form -->
                    <form action="php/contact.php" class="nk-form nk-form-ajax">
                        <div class="row vertical-gap">
                            <div class="col-md-6">
                                <input type="text" class="form-control required" name="name" placeholder="Nome">
                            </div>
                            <div class="col-md-6">
                                <input type="email" class="form-control required" name="email" placeholder="E-mail">
                            </div>
                        </div>

                        <div class="nk-gap-1"></div>
                        <input type="text" class="form-control required" name="title" placeholder="Título">

                        <div class="nk-gap-1"></div>
                        <textarea class="form-control required" name="message" rows="8" placeholder="Comentário" aria-required="true"></textarea>
                        <div class="nk-gap-1"></div>
                        <div class="nk-form-response-success"></div>
                        <div class="nk-form-response-error"></div>
                        <button class="nk-btn">Enviar Mensagem</button>
                    </form>
                    <!-- END: Form -->
                </div>
            </div>
            <div class="nk-gap-5"></div>
        </div>
        <!-- END: Contact Info -->
