<?php
    $infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_site.config`");
    $infoSite->execute();
    $infoSite = $infoSite->fetch();
?>
<!--JS-->
<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.js"></script>

<!--CSS-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.css" rel="stylesheet">


<header class="nk-header">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-autohide nk-navbar-transparent nk-navbar-white-text-on-top">
        <div class="container">
            <div class="nk-nav-table">

                <a href="inicio" class="nk-nav-logo">
                    <img src="<?php echo INCLUDE_PATH; ?>assets/images/logo.png" alt="" width="82"
                         class="nk-nav-logo-onscroll">
                    <img src="<?php echo INCLUDE_PATH; ?>assets/images/fullblack.png" alt="" width="82">
                </a>


                <ul class="nk-nav nk-nav-right nk-nav-icons">


                    <li class="single-icon">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>


<!--
START: Fullscreen Navbar

Additional Classes:
    .nk-navbar-align-center
    .nk-navbar-align-right
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="inicio" class="nk-nav-logo">
                            <img src="<?php echo INCLUDE_PATH; ?>assets/images/logo.png" alt="" width="52">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                                </li>

                                <li class=" nk-drop-item">
                                <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                                </li>
                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank"><i
                                        class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank"><i
                                        class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank"><i
                                        class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Fullscreen Navbar -->
<div class="nk-main">

    <!-- START: Header Title -->
    <!--
Additional Classes:
    .nk-header-title-sm
    .nk-header-title-md
    .nk-header-title-lg
    .nk-header-title-xl
    .nk-header-title-full
    .nk-header-title-parallax
    .nk-header-title-parallax-opacity
-->
    <div class="nk-header-title nk-header-title-md nk-header-title-parallax nk-header-title-parallax-opacity">
        <div class="bg-image">
            <div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $infoSite['img'];?>')"></div>
            <div class="bg-image-overlay" style="background-color: rgba(12, 12, 12, 0.6);"></div>
        </div>
        <div class="nk-header-table">
            <div class="nk-header-table-cell">
                <div class="container">
                    <div class="nk-gap"></div>
                    <div class="nk-header-text text-white" style="color: white;">
                        <div class="lead-sm">
                            <style>
                                h1{
                                    color: white;
                                }
                            </style>
                            <?php echo $infoSite['texto'];?>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

    <!-- END: Header Title -->


    <!-- START: Features -->
    <div class="nk-box bg-white">
        <div class="nk-gap-5 mnt-6"></div>
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-3">
                        <div class="nk-ibox-icon">
                            <img src="<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $infoSite['icone1'];?>" width="50px">
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo_icone1'];?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao1'];?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-3">
                        <div class="nk-ibox-icon">
                            <img src="<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $infoSite['icone2'];?>" width="50px">
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo_icone2'];?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao2'];?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-3">
                        <div class="nk-ibox-icon">
                            <img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['icone3'];?>" width="50px">
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo_icone3'];?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao3'];?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-3">
                        <div class="nk-ibox-icon">
                            <img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['icone4'];?>" width="50px">
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo_icone4'];?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao4'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-3">
                        <div class="nk-ibox-icon">
                            <img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['icone5'];?>" width="50px">
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo_icone5'];?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao5'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-3">
                        <div class="nk-ibox-icon">
                            <img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['icone6'];?>" width="50px">
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo_icone6'];?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao6'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-3">
                        <div class="nk-ibox-icon">
                            <img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['icone7'];?>" width="50px">
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo_icone7'];?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao7'];?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-3">
                        <div class="nk-ibox-icon">
                            <img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['icone8'];?>" width="50px">
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo_icone8'];?></div>
                            <div class="nk-ibox-text">
                                <?php echo $infoSite['descricao8'];?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="nk-gap-5 mnt-6"></div>
    </div>
    <!-- END: Features -->
    <!-- START: Reviews-->
    <div class="nk-box bg-main"  style="height:auto; padding: 10px; display: none">
        <div class="nk-gap-3"></div>
        <div class="nk-gap-3"></div>
        <div class="container-fluid">
            <!-- START:-->
            <div class="nk-carousel nk-carousel-all-visible text-white" data-autoplay="18000" data-dots="true">
                <div class="nk-carousel-inner">
                    <?php
                    $sql = MySql::conectar()->prepare("SELECT * FROM `tb_site.texto` ORDER BY order_id DESC LIMIT 5");
                    $sql->execute();
                    $servicos = $sql->fetchAll();
                    foreach ($servicos as $key => $value) {
                        ?>
                    <div>
                        <div>
                            <blockquote class="nk-blockquote-style-1 text-white">
                                    <p><?php echo $value['texto']; ?></p>
                                <cite><?php echo $value['titulo']; ?></cite>

                            </blockquote>
                            <div class="nk-gap-3 mt-10"></div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- END: Carousel-->
        </div>
        <div class="nk-gap-4 mt-3"></div>
    </div>
    <!-- END: Reviews -->


    <div id="carouselExampleIndicators" class="carousel slide bg-main py-5" data-ride="carousel">
        <?php
        $sql = MySql::conectar()->prepare("SELECT * FROM `tb_site.texto` ORDER BY order_id DESC LIMIT 5");
        $sql->execute();
        $servicos = $sql->fetchAll();
        ?>
        <ol class="carousel-indicators">
            <?php foreach ($servicos as $key => $value) { ?>
                <li data-target="#carouselExampleIndicators" data-slide-to="<?= $key ?>" class="<?php if($key == 0) { echo "active"; } ?>"></li>
            <?php } ?>
        </ol>
        <div class="carousel-inner">
            <?php foreach ($servicos as $key => $value) { ?>
                <div class="carousel-item <?php if($key == 0) { echo "active"; } ?> ">
                    <blockquote class="nk-blockquote-style-1 text-white" style="padding: 10px">
                        <p><?= $value['texto']; ?></p>
                        <cite><?= $value['titulo']; ?></cite>
                    </blockquote>
                    <div class="nk-gap-3 mt-10"></div>
                </div>
            <?php } ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>





            <!-- START: Partners-->
            <div class="bg-white">
                <div class="container">
                    <h2 style="padding-top: 50px; color: black; font-size: 20px; text-align: center"><br>Confira alguns de nossos clientes e
                        empresas atendidas.</h2>
                    <div class="nk-carousel-2 nk-carousel-x4 nk-carousel-no-margin nk-carousel-all-visible">
                        <div class="nk-carousel-inner">
                            <?php
                            $b = MySql::conectar()->prepare("SELECT img FROM `parceiros` ORDER BY order_id DESC LIMIT 10");
                            $b->execute();
                            $servico = $b->fetchAll();
                            foreach ($servico as $key => $value) {
                            ?>
                            <div>
                                <div>
                                    <div class="nk-box-1">

                                        <img src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $value['img']; ?>" alt=""
                                             class="nk-img-fit">
                                    </div>
                                </div>
                            </div>

                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Partners -->

            <!-- START: Need a Project? -->
            <div class="bg-gray-2 text-center">
                <div class="nk-gap-5 mnb-12"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2">
                            <h2>Precisa de um Projeto?</h2>
                            <div class="nk-gap-1 mnt-15"></div>
                            <p>Potencialize a experiência da sua marca.
                                Atendemos você onde estiver. Deixe seu contato e nossa equipe ligará para você. </p>
                            <div class="nk-gap mnt-9"></div>
                            <a href="contato" class="nk-btn nk-btn-long">Entrar em contato</a>
                        </div>
                    </div>
                </div>
                <div class="nk-gap-5"></div>
            </div>
            <!-- END: Need a Project? -->