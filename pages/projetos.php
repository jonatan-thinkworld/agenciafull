<?php
$infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_site.projetos` ");
$infoSite->execute();
$infoSite = $infoSite->fetch();
?>
<header class="nk-header">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-autohide nk-navbar-transparent nk-navbar-white-text-on-top">
        <div class="container">
            <div class="nk-nav-table">

                <a href="inicio" class="nk-nav-logo">
                    <img src="<?php echo INCLUDE_PATH; ?>assets/images/logo.png" alt="" width="82" class="nk-nav-logo-onscroll">
                    <img src="<?php echo INCLUDE_PATH; ?>assets/images/fullblack.png" alt="" width="82">
                </a>


                <ul class="nk-nav nk-nav-right nk-nav-icons">


                    <li class="single-icon">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>


<!--
START: Fullscreen Navbar

Additional Classes:
    .nk-navbar-align-center
    .nk-navbar-align-right
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="inicio" class="nk-nav-logo">
                            <img src="<?php echo INCLUDE_PATH;?>assets/images/logo.png" alt="" width="52">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                                </li>

                                <li class=" nk-drop-item">
                                <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Fullscreen Navbar -->
<div class="nk-main bg-white">

    <!-- START: Header Title -->
    <!--
Additional Classes:
    .nk-header-title-sm
    .nk-header-title-md
    .nk-header-title-lg
    .nk-header-title-xl
    .nk-header-title-full
    .nk-header-title-parallax
    .nk-header-title-parallax-opacity
-->
    <div class="nk-header-title nk-header-title-sm nk-header-title-parallax nk-header-title-parallax-opacity">
        <div class="bg-image">
            <div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $infoSite['capa'];?>')"></div>
            <div class="bg-image-overlay" style="background-color: rgba(20, 20, 20, 0.6);"></div>
        </div>
        <div class="nk-header-table">
            <div class="nk-header-table-cell">
                <div class="container">


                    <div class="nk-gap"></div>
                    <div class="nk-header-text">
                        <h1 class="mb-25 text-white"><?php echo $infoSite['titulo'];?></h1>
                        <div class="lead-sm text-white"><?php echo $infoSite['descricao'];?>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

    <!-- END: Header Title -->


    <div class="container">
        <!-- START: Filter -->

        <ul class="nk-isotope-filter">
            <li class="active" data-filter="*">Todos</li>
            <?php
            $categorias = Painel::selecionarTudo('tb_site.catprojeto');
            foreach ($categorias as $key => $value) {
                ?>
                <li  data-filter="<?php echo $value['nome']; ?>"><?php echo $value['nome']; ?></li>
            <?php } ?>
        </ul>
        <!-- END: Filter -->

        <!-- START: Posts List -->
        <div class="nk-portfolio-list nk-isotope nk-isotope-3-cols nk-popup-gallery">
            <!-- START: Post -->
            <?php
            $query = "SELECT * FROM `tb_site.galeria` ";
            $sql = MySql::conectar()->prepare($query);
            $sql->execute();
            $noticias = $sql->fetchAll();

            foreach($noticias as $key=>$value){
                $sql = MySql::conectar()->prepare("SELECT * FROM `tb_site.catprojeto` WHERE id = ?");
                $sql->execute(array($value['categoria_id']));
                $categoriaNome = $sql->fetch();
                ?>
            <div class="nk-isotope-item" data-filter="<?php echo $categoriaNome['nome'];?>">
                <div class="nk-portfolio-item nk-portfolio-item-square nk-portfolio-item-info-style-1">
                    <a href="<?php echo INCLUDE_PATH?>pagina-unica?id=<?php echo $value['id'];?>">
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $value['img'];?>')"></div>
                        </div>
                        <div class="nk-portfolio-item-info nk-portfolio-item-info-center text-center bg-main">
                            <div>
                                <h2 class="portfolio-item-title h3"><?php echo $value['nome'];?></h2>
                                <div class="portfolio-item-category"><?php echo $categoriaNome['nome'];?></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div><?php } ?>


    <!-- END: Posts List -->

    <div class="nk-gap-5"></div>

</div>
    </div>

<!-- START: Pagination -->
<div class="nk-pagination nk-pagination-center bg-gray-2">
    <a href="#">Ver Mais</a>
</div>
<!-- END: Pagination -->

    </div>
</div>


