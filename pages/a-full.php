<?php
$infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_site.full`");
$infoSite->execute();
$infoSite = $infoSite->fetch();
?>
<link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">
<header class="nk-header">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-autohide nk-navbar-transparent nk-navbar-white-text-on-top">
        <div class="container">
            <div class="nk-nav-table">

                <a href="inicio" class="nk-nav-logo">
                    <img src="<?php echo INCLUDE_PATH;?>assets/images/logo.png" alt="" width="82"
                         class="nk-nav-logo-onscroll">
                    <img src="<?php echo INCLUDE_PATH; ?>assets/images/fullblack.png" alt="" width="82">
                </a>


                <ul class="nk-nav nk-nav-right nk-nav-icons">


                    <li class="single-icon">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>


<!--
START: Fullscreen Navbar

Additional Classes:
    .nk-navbar-align-center
    .nk-navbar-align-right
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="inicio" class="nk-nav-logo">
                            <img src="<?php echo INCLUDE_PATH; ?>assets/images/logo.png" alt="" width="52">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>">Início</a>
                                </li>

                                <li class=" nk-drop-item">
                                <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a>
                                </li>
                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a>
                                </li>

                                <li>
                                    <a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank"><i
                                        class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank"><i
                                        class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank"><i
                                        class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Fullscreen Navbar -->
<div class="nk-main">

    <!-- START: Header Title -->
    <!--
Additional Classes:
    .nk-header-title-sm
    .nk-header-title-md
    .nk-header-title-lg
    .nk-header-title-xl
    .nk-header-title-full
    .nk-header-title-parallax
    .nk-header-title-parallax-opacity
-->
    <div class="nk-header-title nk-header-title-md nk-header-title-parallax nk-header-title-parallax-opacity">
        <div class="bg-image">
            <div style="background-image: url('<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['capa']; ?>');"></div>
            <div class="bg-image-overlay" style="background-color: rgba(12, 12, 12, 0.6);"></div>
        </div>
        <div class="nk-header-table">
            <div class="nk-header-table-cell">
                <div class="container">
                    <div class="nk-gap"></div>
                    <div class="nk-header-text text-white">
                        <div class="lead-sm"><?php echo $infoSite['texto']; ?></div>
                    </div>


                </div>
            </div>
        </div>

    </div>

    <!-- END: Header Title -->





    <!-- START: Features -->
    <div class="nk-box bg-main text-white">
        <div class="nk-gap-5 mnt-6"></div>
        <div class="container">
            <div class="row vertical-gap">
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-1">
                        <div class="nk-ibox-icon">
                            <span class="pe-7s-portfolio"></span>
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo1']; ?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao1']; ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-1">
                        <div class="nk-ibox-icon">
                            <span class="pe-7s-clock"></span>
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo2']; ?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao2']; ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-1">
                        <div class="nk-ibox-icon">
                            <span class="pe-7s-star"></span>
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo2']; ?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao2']; ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="nk-ibox-1">
                        <div class="nk-ibox-icon">
                            <span class="pe-7s-like"></span>
                        </div>
                        <div class="nk-ibox-cont">
                            <div class="nk-ibox-title"><?php echo $infoSite['titulo3']; ?></div>
                            <div class="nk-ibox-text"><?php echo $infoSite['descricao3']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-gap-5 mnt-6"></div>
    </div>
    <!-- END: Features -->

    <!-- START: Our Info Tabs -->
    <div class="nk-box bg-white">
        <div class="nk-gap-5 mnb-6"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <!-- START: Tabs -->
                    <div class="nk-tabs text-center">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab">História</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab">Missão</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab">Princípios</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                                <?php
                                echo $infoSite['historia'];
                                ?>                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                                <?php echo
                                $infoSite['missao'];
                                ?>                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                                <?php echo
                                $infoSite['principio'];
                                ?>                            </div>
                        </div>
                    </div>
                    <!-- END: Tabs -->
                </div>
            </div>
        </div>
        <div class="nk-gap-5 mnt-6"></div>
    </div>
    <!-- END: Our Info Tabs -->

    <!-- START: Info About Me -->
    <div class="container-fluid">
        <div class="row vertical-gap">
            <div class="col-lg-6">
                <div class="bg-image bg-image-parallax" style="background-image: url('<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $infoSite['minha_foto']; ?>');"></div>
                <div class="nk-gap-6"></div>
                <div class="nk-gap-6"></div>
                <div class="nk-gap-6"></div>
                <div class="nk-gap-6"></div>
            </div>
            <div class="col-lg-6">
                <div class="nk-gap-6"></div>
                <div class="nk-gap"></div>
                <div class="mw-620">
                    <p><?php echo $infoSite['bio']?></p>
                </div>
                <div class="nk-gap-3"></div>
                <div class="nk-gap-6"></div>
            </div>
        </div>
    </div>
    <!-- END: Info About Me -->

    <!-- START: Need a Project? -->
    <div class="bg-white text-center">
        <div class="nk-gap-5 mnb-12"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <h2>Precisa de um Projeto?</h2>
                    <div class="nk-gap-1 mnt-15"></div>
                    <p>
                        Potencialize a experiência da sua marca.<br>
                        Atendemos você onde estiver. Deixe seu contato e nossa equipe ligará para você.
                    </p>
                    <div class="nk-gap mnt-9"></div>
                    <a href="contato" class="nk-btn nk-btn-long">Entrar em contato</a>
                </div>
            </div>
        </div>
        <div class="nk-gap-5"></div>
    </div></div>
    <!-- END: Need a Project? -->
