<?php
if (isset($_GET['loggout'])) {
    Painel::loggout();
}
?>
<!DOCTYPE html>
<html lang="PT-BR">
<head>

    <title>FullDesign Login</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?php echo INCLUDE_PATH; ?>assets/images/faviconfull.png">
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/matrix-style.css" />
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/matrix-media.css" />
    <link href="<?php echo INCLUDE_PATH_PAINEL; ?>font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/uniform.css" />


</head>
<body>

<!--Header-part-->
<div id="header">
    <h1><a href="<?php echo INCLUDE_PATH_PAINEL; ?>assets/images/logo.png">FullDesign Login</a></h1>
</div>
<!--close-Header-part-->


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
        <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Bem Vindo <?php echo $_SESSION['nome']; ?></span><b class="caret"></b></a></li>
        <li class=""><a href="<?php echo INCLUDE_PATH?>inicio" target="_blank"><i class="icon icon-home"></i> <span class="text">Ver o Site</span></a></li>
        <li class=""><a href="<?php echo INCLUDE_PATH_PAINEL ?>?loggout"><i class="icon icon-share-alt"></i> <span class="text">Sair</span></a></li>
    </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="<?php INCLUDE_PATH ?>inicio" class="visible-phone"><i class="icon icon-home"></i></a>
    <ul>

        <li <?php selecionadoMenu('');?>><a href="<?php INCLUDE_PATH_PAINEL ?>inicio"><i class="icon icon-home"></i> <span>Painel de controle</span></a> </li>
        <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Conteúdo</span> <span class="label label-important"></span></a>
            <ul>
                <li <?php selecionadoMenu('editar-inicio');?>> <a href="<?php echo INCLUDE_PATH_PAINEL?>editar-inicio">Inicio</a></li>
                <li <?php selecionadoMenu('editar-full');?>> <a  href="<?php echo INCLUDE_PATH_PAINEL?>editar-full">A Full</a></li>
                <li <?php selecionadoMenu('editar-contato');?>> <a  href="<?php echo INCLUDE_PATH_PAINEL?>editar-contato">Contato e Mapa</a></li>
            </ul>
        </li>
        <li <?php selecionadoMenu('listar-parceiro');?>><a href="<?php echo INCLUDE_PATH_PAINEL?>listar-parceiro"><i class="icon icon-file"></i> <span>Parceiros</span> <span class="label label-important"></span></a>
        </li>
        <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Blog</span> <span class="label label-important"></span></a>
            <ul>
                <li <?php selecionadoMenu('editar-blog');?>> <a  href="<?php echo INCLUDE_PATH_PAINEL?>editar-blog">Editar Página principal</a></li>
                <li <?php selecionadoMenu('gerenciar-categoria');?>><a href="<?php echo INCLUDE_PATH_PAINEL?>gerenciar-categoria">Categoria</a></li>
                <li <?php selecionadoMenu('gerenciar-post');?> > <a href="<?php echo INCLUDE_PATH_PAINEL?>gerenciar-post">Posts</a></li>
            </ul>
        </li>
        <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Projetos</span> <span class="label label-important"></span></a>
            <ul>
                <li <?php selecionadoMenu('editar-projetos');?>> <a  href="<?php echo INCLUDE_PATH_PAINEL?>editar-projetos">Editar Página Projetos</a></li>
                <li <?php selecionadoMenu('listar-clientes');?>><a  href="<?php echo INCLUDE_PATH_PAINEL?>listar-clientes">Projeto</a></li>
                <li <?php selecionadoMenu('gerenciar-catprojetos');?>> <a  href="<?php echo INCLUDE_PATH_PAINEL?>gerenciar-catprojetos">Categoria</a></li>
            </ul>
        <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>O Que Fazemos</span> <span class="label label-important"></span></a>
            <ul>
                <li <?php selecionadoMenu('editar-o-que-fazemos');?>> <a  href="<?php echo INCLUDE_PATH_PAINEL?>editar-o-que-fazemos">Editar Página O Que Fazemos</a></li>
                <li <?php selecionadoMenu('listar-texto');?>> <a  href="<?php echo INCLUDE_PATH_PAINEL?>listar-texto">Depoimento</a></li>
            </ul>

        <li class="submenu"> <a href="#"><i class="icon icon-info-sign"></i> <span>Configurações</span> <span class="label label-important"></span></a>
            <ul>
                <li <?php selecionadoMenu('editar-usuario');?>><a  href="<?php echo INCLUDE_PATH_PAINEL?>editar-usuario">Editar Usuário</a></li>
                <li <?php selecionadoMenu('adicionar-usuario');?>> <a  <?php verificaPermissaoMenu(2);?> href="<?php echo INCLUDE_PATH_PAINEL?>adicionar-usuario">Adicionar Usuário</a></li>
            </ul>

        <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>SEO</span> <span class="label label-important"></span></a>
            <ul>
                <li <?php selecionadoMenu('informacoes-do-site');?>> <a href="<?php echo INCLUDE_PATH_PAINEL?>informacoes-do-site?id=1">Informações do Site</a></li>
                <li <?php selecionadoMenu('newsletter');?>><a  href="<?php echo INCLUDE_PATH_PAINEL?>newsletter">Newsletters</a></li>
            </ul>
        </li>


    </ul>
    </li>

    </ul>
</div>
<!--sidebar-menu-->
<div>
    <?php Painel::carregarPagina(); ?>

</div><!--CONTENT-->
<div class="clear"></div>
<!--Footer-part-->
<div class="row-fluid">

    <div id="footer" class="span12">  <?php echo date('Y');?> &copy; Painel Full Design. Desenvolvido por: <a href="">Think World</a> </div>
</div>

<!--end-Footer-part-->
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.ui.custom.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/bootstrap.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.flot.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.flot.resize.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.peity.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/fullcalendar.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.dashboard.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.gritter.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.interface.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.chat.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.validate.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_validation.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.wizard.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.uniform.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/select2.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.popover.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.dataTables.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.tables.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/wysihtml5-0.3.0.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/bootstrap-colorpicker.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/bootstrap-datepicker.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.toggle.buttons.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/masked.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/wysihtml5-0.3.0.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/jquery.peity.min.js"></script>
<script type="text/javascript">
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage (newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-" ) {
                resetMenu();
            }
            // else, send page to designated URL
            else {
                document.location.href = newURL;
            }
        }
    }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ydz76vz26f45p2fd6xdo6soxz9l7jmfjk9igb024qwf6p79i"></script>
<script>tinymce.init({
        selector:'.tinymce',
        plugins: "image textcolor media tabfocus textpattern advcode directionality",
        toolbar: "image forecolor backcolor media tabfocus textpattern code ltr rtl",
        textpattern_patterns: [
            {start: '*', end: '*', format: 'italic'},
            {start: '**', end: '**', format: 'bold'},
            {start: '#', format: 'h1'},
            {start: '##', format: 'h2'},
            {start: '###', format: 'h3'},
            {start: '####', format: 'h4'},
            {start: '#####', format: 'h5'},
            {start: '######', format: 'h6'},
            {start: '1. ', cmd: 'InsertOrderedList'},
            {start: '* ', cmd: 'InsertUnorderedList'},
            {start: '- ', cmd: 'InsertUnorderedList'}
        ]

});</script>
</body>
</html>
