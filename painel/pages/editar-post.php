<?php
if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    $post = Painel::selecionar('tb_site.blog', 'id = ?', array($id));

} else {
    Painel::alerta('erro', 'Você precisa passar o parametro ID!');
    die();
}
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Blog</a> <a href="cadastrar-post" class="current">Editar Post</a></div>
        <h1>Editar Post</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Personal-info</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php
                            if (isset($_POST['acao'])) {
                                //enviei o meu formulário
                                //Painel::alerta('sucesso','Cadastro realizado com sucesso!');
                                $titulo = addslashes($_POST['titulo']);
                                $conteudo = addslashes($_POST['conteudo']);
                                $imagem = $_FILES['capa'];
                                $imagem_atual = $_POST['imagem_atual'];
                                $verifica = MySql::conectar()->prepare("SELECT `ID` FROM `tb_site.blog` WHERE titulo = ? AND categoria_id = ? AND id != ?");
                                $verifica->execute(array($titulo, $_POST['categoria_id'], $id));
                                if ($verifica->rowCount() == 0) {
                                    if ($imagem['name'] != '') {
                                        //EXISTE O UPLOAD DE IMAGEM
                                        if (Painel::imagemValida($imagem)) {
                                            Painel::deleteArquivo($imagem_atual);
                                            $imagem = Painel::uploadArquivo($imagem);
                                            $slug = Painel::geradorSlug($titulo);
                                            $arr = ['titulo' => $titulo, 'date' => date('Y-m-d'), 'categoria_id' => $_POST['categoria_id'], 'conteudo' => $conteudo, 'capa' => $imagem, 'slug' => $slug, 'id' => $id, 'nome_tabela' => 'tb_site.blog'];
                                            Painel::atualizar($arr);
                                            $post = Painel::selecionar('tb_site.blog', 'id = ?', array($id));
                                            Painel::alerta('sucesso', 'O post foi atualizado junto com a imagem');
                                        } else {
                                            Painel::alerta('erro', 'O formato da imagem não é válido!');
                                        }
                                    } else {
                                        $imagem = $imagem_atual;
                                        $slug = Painel::geradorSlug($titulo);
                                        $arr = ['titulo' => $titulo, 'date' => date('Y-m-d'), 'categoria_id' => $_POST['categoria_id'], 'conteudo' => $conteudo, 'capa' => $imagem, 'slug' => $slug, 'id' => $id, 'nome_tabela' => 'tb_site.blog'];
                                        Painel::atualizar($arr);
                                        $post = Painel::selecionar('tb_site.blog', 'id = ?', array($id));
                                        Painel::alerta('sucesso', 'O post foi atualizados com a imagem antiga');
                                    }
                                } else {
                                    Painel::alerta('erro', 'Já existe uma notícia com esse título');

                                }
                            }

                            ?>

                            <div class="control-group">
                                <label class="control-label">Título</label>
                                <div class="controls">
                                    <input type="text" name="titulo" class="span11"
                                           value="<?php echo $post['titulo']; ?>">
                                </div>
                            </div>


                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"><span class="icon"> <i
                                                    class="icon-align-justify"></i> </span>
                                        <h5>Postagem:</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">

                                            <textarea class="tinymce span12" rows="40"
                                                      name="conteudo"> <?php echo $post['conteudo']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Selecione a Categoria</label>
                                <div class="controls">
                                    <select name="categoria_id">
                                        <?php
                                        $categorias = Painel::selecionarTudo('tb_site.categoria');
                                        foreach ($categorias as $key => $value) {
                                            ?>
                                            <option <?php if ($value['id'] == $post['categoria_id']) echo 'selected'; ?>
                                                    value="<?php echo $value['id'] ?>"><?php echo $value['nome']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Capa Atual</label>
                                <div class="controls">
                                    <img style="width: 200px; height: 150px;"
                                         src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $post['capa']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Nova Capa</label>
                                <div class="controls">
                                    <input type="file" name="capa"/>
                                    <input type="hidden" name="imagem_atual" value="<?php echo $post['capa']; ?>">

                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Publicar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

