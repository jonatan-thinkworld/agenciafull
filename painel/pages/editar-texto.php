<?php
//verifica permissão para entrar na página
verificaPermissaoPagina(2);

if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    $depoimento = Painel::selecionar('tb_site.texto', 'id = ?', array($id));

} else {
    Painel::alerta('erro', 'Você precisa passar o parametro ID!');
    die();
}

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Blog</a> <a href="cadastrar-post" class="current">Publicar Post</a></div>
        <h1>Cadastrar Depoimento</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post"  class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                if (Painel::atualizar($_POST)) {
                                    Painel::alerta('sucesso', 'O texto foi editado com sucesso!');
                                    $depoimento = Painel::selecionar('tb_site.texto', 'id = ?', array($id));
                                }else{
                                    Painel::alerta('erro', 'Campos vázios não são permitidos!');
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Nome do Cliente:</label>
                                <div class="controls">
                                    <input type="text" name="titulo" class="span10" value="<?php echo $depoimento['titulo']; ?>" required>
                                </div>
                            </div>



                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                        <h5>Conteúdo:</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">
                                            <textarea class="tinymce span12" rows="15" name="texto"> <?php echo $depoimento['texto']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="hidden" name="id" value="<?php echo $id;?>">
                                <input type="hidden" name="nome_tabela" value="tb_site.texto">
                                <input type="submit" class="btn btn-success" name="acao" value="Cadartrar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

