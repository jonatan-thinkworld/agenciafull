<?php
$infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_site.config` ");
$infoSite->execute();
$infoSite = $infoSite->fetchall();
?>
<div class="box-content">
    <h2><i class="fa fa-id-card"></i> Editar Site </h2>
    <div class="wraper-table">
        <table>
            <tr>
                <td>Página:</td>
                <td></td>
            </tr>
            <?php
            foreach ($infoSite as $key => $value) {
                ?>
                <tr>
                    <td><?php echo $value['nome']; ?></td>
                    <td><a class="btn edit" href="<?php echo INCLUDE_PATH_PAINEL ?>editar-pagina?id=<?php echo $value['id'];?>"><i class="fa fa-pencil"></i> Editar</a></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
</div><!--BOX-CONTENT-->