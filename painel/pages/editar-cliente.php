<?php
if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    $slide = Painel::selecionar('tb_site.galeria', 'id = ?', array($id));
    $galeria = Painel::selecionar('tb_admin_imagem', 'id_produto = ?', array($id));
    $videos = Painel::selecionar('tb_site.video','video_id = ?',array($id));
}
if(isset($_GET['excluir']) && isset($_GET['id_usuario'])){
    $idExcluir = intval($_GET['excluir']);
    $id_usuario = intval($_GET['id_usuario']);
    $selecionarImagem = MySql::conectar()->prepare("SELECT img FROM `tb_admin_imagem` WHERE id = ?");
    $selecionarImagem->execute(array($_GET['excluir']));
    $imagem = $selecionarImagem->fetch()['img'];
    Painel::deleteArquivo($imagem);
    Painel::deletar('tb_admin_imagem',$idExcluir);

    Painel::redirecionar(INCLUDE_PATH_PAINEL. "editar-cliente?id={$id_usuario}");

}elseif (isset($_GET['order']) && isset($_GET['id'])){
    Painel::ordernarItem('tb_admin_imagem',$_GET['order'],$_GET['id']);
}
$paginaAtual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
$porPagina = 10;
$clientes = Painel::selecionarTudo('tb_admin_imagem',($paginaAtual - 1)* $porPagina,$porPagina);

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
           <a href="<?php INCLUDE_PATH_PAINEL ?>listar-clientes"
                                                        class="tip-bottom">Listar Projetos</a><a
                    href="<?php INCLUDE_PATH_PAINEL ?>" class="current">Editar Projetos</a></div>
        <h1>Editar Projetos</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                //enviei o meu formulário
                                //Painel::alerta('sucesso','Cadastro realizado com sucesso!');
                                $nome = addslashes($_POST['nome']);
                                $video = addslashes($_POST['video']);
                                $categoria_id = $_POST['categoria_id'];
                                $pagina = addslashes($_POST['conteudo']);
                                $imagem = $_FILES['imagem'];
                                $imagem_atual = $_POST['imagem_atual'];
                                if ($video!= '') {
                                    $arr = ['url' => $video, 'video_id'=>$id,'order_id' => '0', 'id' => $videos['id'], 'nome_tabela' => 'tb_site.video'];
                                    Painel::atualizar($arr);
                                    $videos = Painel::selecionar('tb_site.video','video_id = ?',array($id));
                                }
                                if (isset($_POST['destaque'])) {
                                    $destaque = $_POST['destaque'];
                                } else {
                                    $destaque = 'não';
                                }
                                if ($imagem['name'] != '') {
                                    //EXISTE O UPLOAD DE IMAGEM
                                    if (Painel::imagemValida($imagem)) {
                                        Painel::deleteArquivo($imagem_atual);
                                        $imagem = Painel::uploadArquivo($imagem);
                                        $slug = Painel::geradorSlug($nome);
                                        $arr = ['nome' => $nome, 'categoria_id' => $categoria_id, 'conteudo' => $pagina, 'img' => $imagem, 'order_id' => '0', 'slug' => $slug, 'destaque' => $destaque, 'id' => $id, 'nome_tabela' => 'tb_site.galeria'];
                                        Painel::atualizar($arr);
                                        $slide = Painel::selecionar('tb_site.galeria', 'id = ?', array($id));
                                        Painel::alerta('sucesso', 'Os dados foram atualizados junto com a imagem');
                                    } else {
                                        Painel::alerta('erro', 'O formato da imagem não é válido!');
                                    }
                                }  else {
                                    $imagem = $imagem_atual;
                                    $slug = Painel::geradorSlug($nome);
                                    $arr = ['nome' => $nome, 'categoria_id' => $categoria_id, 'conteudo' => $pagina, 'img' => $imagem, 'order_id' => '0', 'slug' => $slug, 'destaque' => $destaque, 'id' => $id, 'nome_tabela' => 'tb_site.galeria'];
                                    Painel::atualizar($arr);
                                    $slide = Painel::selecionar('tb_site.galeria', 'id = ?', array($id));
                                    Painel::alerta('sucesso', 'Os dados foram atualizados com a imagem antiga');
                                }
                            }
                            ?>

                            <div class="control-group">
                                <label class="control-label">Categoria</label>
                                <div class="controls">
                                    <input type="text" name="nome" class="span11" value="<?php echo $slide['nome']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Selecione a Categoria do Projeto</label>
                                <div class="controls">
                                    <select name="categoria_id">
                                        <?php
                                        $categorias = Painel::selecionarTudo('tb_site.catprojeto');
                                        foreach ($categorias as $key => $value) {
                                            ?>
                                            <option <?php if($value['id'] == @$_POST['categoria_id']) echo 'selected'; ?> value="<?php echo $value['id'] ?>"><?php echo $value['nome']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <td><a class="btn btn-warning" href="<?php echo INCLUDE_PATH_PAINEL ?>cadastrar-categoria-projeto"><i class="icon-pencil"></i> Cadastrar Categoria</a></td>

                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Destacar na Página Inicial:</label>
                                <div class="controls">
                                   <?php if($slide['destaque'] == 'sim'){?>
                                    <input type="checkbox" name="destaque" value="sim" checked>
                                    <?php } else{ ?>
                                    <input type="checkbox" name="destaque" value="sim">
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"><span class="icon"> <i
                                                    class="icon-align-justify"></i> </span>
                                        <h5>Descrição:</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">

                                            <textarea class="tinymce span12" rows="40"
                                                      name="conteudo"><?php echo $slide['conteudo']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Carregar Nova Imagem de Destaque</label>
                                <div class="controls">
                                    <input type="file" name="imagem"/>
                                    <input type="hidden" name="imagem_atual" value="<?php echo $slide['img']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Imagem de Destaque</label>
                                <div class="controls">
                                    <td><img style="width: 80px; height: 80px;"
                                             src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $slide['img']; ?>">
                                    </td>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Carregar Novas Imagens</label>
                                <div class="controls">
                                    <a class="btn btn-info"
                                       href="<?php echo INCLUDE_PATH_PAINEL ?>carregar-imagens?id=<?php echo $slide['id']; ?>"><i
                                                class="fa fa-times"></i> Cadastrar mais imagens</a></td>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Link do vídeo</label>
                                <div class="controls">
                                    <input type="text" name="video" class="span11" value="<?php echo $videos['url']; ?>">

                                    </div>
                            </div>


                            <div class="control-group">

                                <div class="controls">
                                    <?php

                                    {
                                        ?>
                                        <td>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                                            <h5>Listar todas as imagens</h5>
                                        </div>
                                        <div class="widget-content nopadding">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <?php
                                                $infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_admin_imagem` WHERE `id_produto` = $id ");
                                                $infoSite->execute();
                                                $infoSite = $infoSite->fetchAll();
                                                foreach ($infoSite as $key => $value) {
                                                ?>
                                                <tbody>
                                                <tr class="odd gradeX">
                                                    <td><img style="width: 80px; height: 80px;" src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $value['img']; ?>"></td>
                                                    <td><a class="btn btn-warning" href="<?php echo INCLUDE_PATH_PAINEL ?>editar-imagem?id=<?php echo $value['id'];?>"><i class="icon-pencil"></i> Editar</a></td>
                                                    <td><a actionBtn="delete" class="btn btn-danger" href="<?php echo INCLUDE_PATH_PAINEL ?>editar-cliente?id_usuario=<?php echo $id ;?>&excluir=<?php echo $value['id']; ?>"><i class="icon-remove"></i> Excluir</a></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <input type="submit" class="btn btn-success" name="acao" value="Atualizar!">
                                    </div>




                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

