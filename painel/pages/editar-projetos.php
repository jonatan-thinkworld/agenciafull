<?php
$post = Painel::selecionar('tb_site.projetos');

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Site</a> <a href="<?php INCLUDE_PATH_PAINEL ?>" class="current">Editar Página Projetos</a></div>
        <h1>Editar Página Projetos</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Personal-info</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php
                            if (isset($_POST['acao'])) {
                                //enviei o meu formulário
                                //Painel::alerta('sucesso','Cadastro realizado com sucesso!');
                                $titulo = addslashes($_POST['titulo']);
                                $conteudo = addslashes($_POST['conteudo']);
                                $imagem = $_FILES['capa'];
                                $imagem_atual = $_POST['imagem_atual'];
                                if ($imagem['name'] != '') {
                                    //EXISTE O UPLOAD DE IMAGEM
                                    if (Painel::imagemValida($imagem)) {
                                        Painel::deleteArquivo($imagem_atual);
                                        $imagem = Painel::uploadArquivo($imagem);
                                        $arr = ['capa' => $imagem, 'titulo' => $titulo, 'descricao' => $conteudo, 'id' => '1', 'nome_tabela' => 'tb_site.projetos'];
                                        Painel::atualizar($arr);
                                        $post = Painel::selecionar('tb_site.projetos');
                                        Painel::alerta('sucesso', 'A página foi atualizada junto com a imagem');
                                    } else {
                                        Painel::alerta('erro', 'O formato da imagem não é válido!');
                                    }
                                } else {
                                    $imagem = $imagem_atual;
                                    $arr = ['capa' => $imagem, 'titulo' => $titulo, 'descricao' => $conteudo, 'id' => '1', 'nome_tabela' => 'tb_site.projetos'];
                                    Painel::atualizar($arr);
                                    $post = Painel::selecionar('tb_site.projetos');
                                    Painel::alerta('sucesso', 'A página foi atualizada com a imagem antiga');
                                }
                            }

                            ?>


                            <div class="control-group">
                                <label class="control-label">Capa Atual</label>
                                <div class="controls">
                                    <img style="width: 200px; height: 150px;"
                                         src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $post['capa']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Nova Capa</label>
                                <div class="controls">
                                    <input type="file" name="capa"/>
                                    <input type="hidden" name="imagem_atual" value="<?php echo $post['capa']; ?>">

                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Título</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo" value="<?php echo $post['titulo']; ?>">
                                </div>
                            </div>



                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"><span class="icon"> <i
                                                    class="icon-align-justify"></i> </span>
                                        <h5>Conteúdo</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">

                                            <textarea class="tinymce span12" rows="25"
                                                      name="conteudo"><?php echo $post['descricao']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Publicar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

