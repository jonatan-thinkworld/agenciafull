<?php
if(isset($_GET['excluir'])){
    $idExcluir = intval($_GET['excluir']);
    $selecionarImagem = MySql::conectar()->prepare("SELECT * FROM `newsletter`");
    $selecionarImagem->execute(array($_GET['excluir']));

    Painel::deletar('tb_site.categoria',$idExcluir);
    Painel::redirecionar(INCLUDE_PATH_PAINEL.'listar-clientes');
}elseif (isset($_GET['order']) && isset($_GET['id'])){
    Painel::ordernarItem('newsletter',$_GET['order'],$_GET['id']);
}
$paginaAtual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
$porPagina = 10;
$clientes = Painel::selecionarTudo('newsletter',($paginaAtual - 1)* $porPagina,$porPagina);

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php INCLUDE_PATH_PAINEL?>/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Inicio</a> <a href="<?php INCLUDE_PATH_PAINEL?>" >SEO</a> <a href="<?php INCLUDE_PATH_PAINEL?>" class="current">Gerencia Newsletter</a> </div>
        <h1>Listar E-mails</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
                        <h5>Gerenciar Newsletter</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>E-mail</th>

                            </tr>
                            </thead>
                            <?php
                            foreach ($clientes as $key => $value) {
                            ?>
                            <tbody>
                            <tr class="odd gradeX">
                                <td  style="text-align: center;"><?php echo $value['email']; ?></td>


                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>


                </div>
                <div class="pagination alternate">
                    <ul>
                        <?php
                        $totalPaginas= ceil(count(Painel::selecionarTudo('tb_site.categoria'))/$porPagina);

                        for ($i=1; $i <= $totalPaginas; $i++){
                            if($i == $paginaAtual)
                                echo '<li><a class="page-selected" href="'.INCLUDE_PATH_PAINEL.'gerenciar-categoria?pagina='.$i.'">'.$i.'</a></li></div>';
                            else
                                echo '<li><a href="'.INCLUDE_PATH_PAINEL.'gerenciar-categoria?pagina='.$i.'">'.$i.'</a></li></div>';

                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div></div>
</div>


