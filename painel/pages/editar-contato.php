<?php
//verifica permissão para entrar na página
verificaPermissaoPagina(2);
$depoimento = Painel::selecionar('contato');

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Site</a> <a href="<?php INCLUDE_PATH_PAINEL ?>" class="current">Editar Contato</a></div>
        <h1>Editar Contato</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post"  class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                $titulo = addslashes($_POST['titulo']);
                                $contato = addslashes($_POST['contato']);
                                if($titulo == '' || $contato == '') {
                                    Painel::alerta('erro', 'Campos vázios não são permitidos!');
                                    Painel::selecionar('contato');
                                }else{
                                    $arr = ['titulo'=>$titulo, 'contato'=>$contato, 'id'=>'1','nome_tabela'=>'contato'];
                                    Painel::atualizar($arr);
                                    Painel::alerta('sucesso', 'Cadastro realizado com sucesso!');
                                    $depoimento = Painel::selecionar('contato');
                                }
                                $depoimento = Painel::selecionar('contato');
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Categoria</label>
                                <div class="controls">
                                    <input type="text" name="titulo" class="span11" value="<?php echo $depoimento['titulo']; ?>">
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                        <h5>Descrição:</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">
                                            <textarea class="tinymce span12" rows="40" name="contato"><?php echo $depoimento['contato']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-actions">
                                <input type="hidden" name="id" value="<?php echo $id;?>">
                                <input type="hidden" name="nome_tabela" value="contato">
                                <input type="submit" class="btn btn-success" name="acao" value="Atualizar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

