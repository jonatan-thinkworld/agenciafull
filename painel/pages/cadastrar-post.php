<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="tip-bottom">Blog</a> <a href="cadastrar-post" class="current">Publicar Post</a> </div>
        <h1>Cadastrar Post</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Personal-info</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php

                            if(isset($_POST['acao'])){
                                $categoria_id = $_POST['categoria_id'];
                                $titulo = $_POST['titulo'];
                                $conteudo = $_POST['conteudo'];
                                $capa = $_FILES['capa'];

                                if($titulo == '' || $conteudo == ''){
                                    Painel::alerta('erro','Campos Vázios não são permitidos!');
                                }else if($capa['tmp_name'] == '' ){
                                    Painel::alerta('erro','A imagem de capa precisa ser selecionada.');
                                }else{
                                    if(Painel::imagemValida($capa)){
                                        $verifica = MySql::conectar()->prepare("SELECT * FROM `tb_site.blog` WHERE titulo=? AND categoria_id = ?");
                                        $verifica->execute(array($titulo,$categoria_id));
                                        if($verifica->rowCount() == 0){
                                            $imagem = Painel::uploadArquivo($capa);
                                            $slug = Painel::geradorSlug($titulo);
                                            $arr = ['categoria_id'=>$categoria_id,'date'=>date('Y-m-d'),'titulo'=>$titulo,'conteudo'=>$conteudo,'capa'=>$imagem,'slug'=>$slug,
                                                'order_id'=>'0',
                                                'nome_tabela'=>'tb_site.blog'
                                            ];
                                            if(Painel::inserir($arr)){
                                                Painel::redirecionar(INCLUDE_PATH_PAINEL.'cadastrar-post?sucesso');
                                            }

                                            //Painel::alert('sucesso','O cadastro da notícia foi realizado com sucesso!');
                                        }else{
                                            Painel::alerta('erro','Já existe uma notícia com esse nome!');
                                        }
                                    }else{
                                        Painel::alerta('erro','Selecione uma imagem válida!');
                                    }

                                }

                            }
                            if(isset($_GET['sucesso']) && !isset($_POST['acao'])){
                                Painel::alerta('sucesso','O Post foi publicado com sucesso!');
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Selecione a Categoria</label>
                                <div class="controls">
                                    <select name="categoria_id">
                                        <?php
                                        $categorias = Painel::selecionarTudo('tb_site.categoria');
                                        foreach ($categorias as $key => $value) {
                                            ?>
                                            <option <?php if($value['id'] == @$_POST['categoria_id']) echo 'selected'; ?> value="<?php echo $value['id'] ?>"><?php echo $value['nome']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Título</label>
                                <div class="controls">
                                    <input type="text" name="titulo" class="span11" value="<?php recoverPost('titulo'); ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Carregar Capa</label>
                                <div class="controls">
                                    <input type="file" name="capa"/>
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                        <h5>Postagem:</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">

                                            <textarea class="tinymce span12" rows="40" name="conteudo"><?php recoverPost('conteudo'); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <input type="hidden" name="order_id" value="0">
                                <input type="hidden" name="nome_tabela" value="tb_site.blog" />
                                <input type="submit" class="btn btn-success" name="acao" value="Publicar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div></div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

