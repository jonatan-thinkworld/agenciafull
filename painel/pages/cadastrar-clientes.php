<?php
verificaPermissaoPagina(2)
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Projeto e Depoimento</a> <a href="listar-clientes" class="tip-bottom">Projeto</a><a href="<?php INCLUDE_PATH_PAINEL ?>cadastrar-clientes"
                                                           class="current">Cadastrar Projeto</a></div>
        <h1>Cadastrar Projeto</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php
                            if (isset($_POST['acao'])) {
                                //enviei o meu formulário
                                $nome = addslashes($_POST['nome']);
                                $categoria_id = $_POST['categoria_id'];
                                $pagina = $_POST['conteudo'];
                                $imagem = $_FILES['imagem'];
                                $galeria = $_FILES['galeria'];
                                $video = addslashes($_POST['video']);
                                if (isset($_POST['destaque'])) {
                                    $destaque = $_POST['destaque'];
                                } else {
                                    $destaque = 'não';
                                }


                                //VALIDAÇÃO DE CADASTRO
                                if ($nome == '') {
                                    Painel::alerta('erro', 'O nome do cliente está vazio!');
                                } elseif ($imagem['name'] == '') {
                                    Painel::alerta('erro', 'As imagens precisam estar selecionada!');
                                } else {
                                    //PODEMOS CADASTRAR!
                                    if (cliente::paginaExiste($nome)) {
                                        Painel::alerta('erro', 'A página já existe ');
                                    } else {
                                        if (isset($_FILES['imagem'])) {
                                            $imagem = Painel::uploadArquivo($imagem);
                                            $slug = Painel::geradorSlug($nome);
                                            $arr = ['nome' => $nome, 'categoria_id'=>$categoria_id, 'conteudo' => $pagina, 'img' => $imagem, 'order_id' => '0', 'slug' => $slug, 'destaque' => $destaque, 'nome_tabela' => 'tb_site.galeria'];
                                            Painel::inserir($arr);
                                        }
                                        if (isset($_FILES['galeria'])) {
                                            if (count($_FILES['galeria']) > 0) {
                                                for ($q = 0; $q < count($_FILES['galeria']); $q++) {
                                                    $infoSite = MySql::conectar()->prepare("SELECT id FROM `tb_site.galeria` ORDER BY id DESC limit 1;");
                                                    $infoSite->execute();
                                                    $infoSite = $infoSite->fetch();
                                                    $id = $infoSite['id'];
                                                    @$nomedoarquivo = md5($_FILES['galeria']['name'][$q] . time() . rand(0, 999)) . '.jpg';
                                                    move_uploaded_file(@$_FILES['galeria']['tmp_name'][$q], BASE_DIR_PAINEL . '/uploads/' . $nomedoarquivo);
                                                    $a = ['id_produto' => $id, 'img' => $nomedoarquivo,'order_id'=>'0', 'nome_tabela' => 'tb_admin_imagem'];
                                                    Painel::inserir($a);
                                                }
                                            }


                                        }
                                        if($video != '') {
                                            $arr = ['url' => $video, 'video_id' => $id,'order_id'=>'0', 'nome_tabela' => 'tb_site.video'];
                                            Painel::inserir($arr);
                                        }


                                    }
                                    Painel::alerta('sucesso', 'O cadastro do cliente foi realizado com sucesso!');
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Nome do Cliente</label>
                                <div class="controls">
                                    <input type="text" name="nome" class="span11" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Selecione a Categoria do Projeto</label>
                                <div class="controls">
                                    <select name="categoria_id">
                                        <?php
                                        $categorias = Painel::selecionarTudo('tb_site.catprojeto');
                                        foreach ($categorias as $key => $value) {
                                            ?>
                                            <option <?php if($value['id'] == @$_POST['categoria_id']) echo 'selected'; ?> value="<?php echo $value['id'] ?>"><?php echo $value['nome']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <td><a class="btn btn-warning" href="<?php echo INCLUDE_PATH_PAINEL ?>cadastrar-categoria-projeto"><i class="icon-pencil"></i> Cadastrar Categoria</a></td>

                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Destacar na Página Inicial:</label>
                                <div class="controls">
                                    <input type="checkbox" name="destaque" value="sim">
                                </div>
                            </div>


                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"><span class="icon"> <i
                                                    class="icon-align-justify"></i> </span>
                                        <h5>Conteúdo:</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">
                                            <textarea class="tinymce span12" rows="15"
                                                      name="conteudo"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Carregar Capa</label>
                                <div class="controls">
                                    <input type="file" name="imagem"/>
                                    <input type="hidden" name="imagem" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Carregar Imagens</label>
                                <div class="controls">
                                    <input type="file" name="galeria[]" multiple/>
                                    <input type="hidden" name="galeria" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Link do Vídeo</label>
                                <div class="controls">
                                    <input type="text" name="video" class="span11">
                                </div>
                            </div>




                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Cadastrar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

