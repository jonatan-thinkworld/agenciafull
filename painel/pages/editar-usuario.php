<?php
verificaPermissaoPagina(2)
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Início</a>
            <a href="#" class="tip-bottom">Administração</a> <a href="cadastrar-post" class="current">Editar Usuarios</a></div>
        <h1>Editar Usuarios</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Personal-info</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php
                            if (isset($_POST['acao'])) {
                                //enviei o meu formulário
                                //Painel::alerta('sucesso','Cadastro realizado com sucesso!');
                                $nome = addslashes($_POST['nome'])  ;
                                $senha = md5($_POST['password']);
                                $imagem = $_FILES['imagem'];
                                $imagem_atual = $_POST['imagem_atual'];
                                $usuario = new Usuario();

                                if ($imagem['name'] != '') {
                                    //EXISTE O UPLOAD DE IMAGEM
                                    if (Painel::imagemValida($imagem)) {
                                        Painel::deleteArquivo($imagem_atual);
                                        $imagem = Painel::uploadArquivo($imagem);
                                        if ($usuario->atualizarUsuario($nome, $senha, $imagem)) {
                                            $_SESSION['img'] = $imagem;
                                            Painel::alerta('sucesso', 'Atualizado com Sucesso!');
                                        } else {
                                            Painel::alerta('erro', 'Ocorreu um erro ao atualizar!');
                                        }
                                    }else{
                                        Painel::alerta('erro', 'O formato da imagem não é válido!');
                                    }
                                } else {
                                    $imagem = $imagem_atual;
                                    if ($usuario->atualizarUsuario($nome, $senha, $imagem)) {
                                        Painel::alerta('sucesso', 'Atualizado com sucesso!');
                                    } else {
                                        Painel::alerta('erro', 'Ocorreu um erro ao atualizar!');
                                    }
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Nome</label>
                                <div class="controls">
                                    <input type="text" name="nome" class="span11" required value="<?php echo $_SESSION['nome']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Senha</label>
                                <div class="controls">
                                    <input type="password" class="span11" name="password" value="<?php echo $_SESSION['password']; ?>" required>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Carregar Imagem</label>
                                <div class="controls">
                                    <input type="file" name="imagem"/>
                                    <input type="hidden" name="imagem_atual" value="<?php echo $_SESSION['img']; ?>">
                                </div>
                            </div>

                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Cadartrar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

