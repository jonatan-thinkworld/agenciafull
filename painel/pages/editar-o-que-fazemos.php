<?php
//verifica permissão para entrar na página
verificaPermissaoPagina(2);
$depoimento = Painel::selecionar('tb_site.config');

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Site</a> <a href="cadastrar-post" class="current">Editar O Que Fazemos</a></div>
        <h1>Editar Página</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                $texto_titlo = $_POST['texto_titulo'];
                                $titulo_icone1 = addslashes($_POST['titulo_icone1']);
                                $descricao1 = addslashes($_POST['descricao1']);
                                $titulo_icone2 = addslashes($_POST['titulo_icone2']);
                                $descricao2 = addslashes($_POST['descricao2']);
                                $titulo_icone3 = addslashes($_POST['titulo_icone3']);
                                $descricao3 = addslashes($_POST['descricao3']);
                                $titulo_icone4 = addslashes($_POST['titulo_icone4']);
                                $descricao4 = addslashes($_POST['descricao4']);
                                $titulo_icone5 = addslashes($_POST['titulo_icone5']);
                                $descricao5 = addslashes($_POST['descricao5']);
                                $titulo_icone6 = addslashes($_POST['titulo_icone6']);
                                $descricao6 = addslashes($_POST['descricao6']);
                                $titulo_icone7 = addslashes($_POST['titulo_icone7']);
                                $descricao7 = addslashes($_POST['descricao7']);
                                $titulo_icone8 = addslashes($_POST['titulo_icone8']);
                                $descricao8 = addslashes($_POST['descricao8']);
                                $capa = $_FILES['capa'];
                                $capa_atual = $_POST['capa_atual'];
                                if ($texto_titlo == '' || $titulo_icone1 == '' || $descricao1 == '' || $titulo_icone2 == '' || $descricao2 == ''
                                    || $titulo_icone3 == '' || $descricao3 == '' || $titulo_icone4 == '' || $descricao4 == '' || $titulo_icone5 == ''
                                    || $descricao5 == '' || $titulo_icone6 == '' || $descricao6 == '' || $titulo_icone7 == '' || $descricao7 == ''
                                    || $titulo_icone8 == '' || $descricao8 == '') {
                                    Painel::alerta('erro', 'Campos vázios não são permitidos!');
                                    Painel::selecionar('tb_site.config');
                                } else {
                                    if ($capa['name'] != '') {
                                        Painel::deleteArquivo($capa_atual);
                                        $capa = Painel::uploadArquivo($capa);
                                        $arr = ['texto' => $texto_titlo, 'img' => $capa, 'titulo_icone1' => $titulo_icone1, 'descricao1' => $descricao1, 'titulo_icone2' => $titulo_icone2, 'descricao2' => $descricao2, 'titulo_icone3' => $titulo_icone3, 'descricao3' => $descricao3, 'titulo_icone4' => $titulo_icone4, 'descricao4' => $descricao4, 'titulo_icone5' => $titulo_icone5, 'descricao5' => $descricao5, 'titulo_icone6' => $titulo_icone6, 'descricao6' => $descricao6, 'titulo_icone7' => $titulo_icone7, 'descricao7' => $descricao7, 'titulo_icone8' => $titulo_icone8, 'descricao8' => $descricao8, 'id' => '1', 'nome_tabela' => 'tb_site.config'];
                                        Painel::atualizar($arr);
                                        Painel::alerta('sucesso', 'Página atualizada com sucesso!');
                                        $depoimento = Painel::selecionar('tb_site.conf');
                                    } else {
                                        $capa = $capa_atual;
                                        $arr = ['texto' => $texto_titlo, 'img' => $capa, 'titulo_icone1' => $titulo_icone1, 'descricao1' => $descricao1, 'titulo_icone2' => $titulo_icone2, 'descricao2' => $descricao2, 'titulo_icone3' => $titulo_icone3, 'descricao3' => $descricao3, 'titulo_icone4' => $titulo_icone4, 'descricao4' => $descricao4, 'titulo_icone5' => $titulo_icone5, 'descricao5' => $descricao5, 'titulo_icone6' => $titulo_icone6, 'descricao6' => $descricao6, 'titulo_icone7' => $titulo_icone7, 'descricao7' => $descricao7, 'titulo_icone8' => $titulo_icone8, 'descricao8' => $descricao8, 'id' => '1', 'nome_tabela' => 'tb_site.config'];
                                        Painel::atualizar($arr);
                                        Painel::alerta('sucesso', 'Página atualizada com sucesso!');
                                        $depoimento = Painel::selecionar('tb_site.config');

                                    }
                                }
                                $depoimento = Painel::selecionar('tb_site.config');
                            }
                            ?>

                            <div class="control-group">
                                <label class="control-label">Carregar Nova capa</label>
                                <div class="controls">
                                    <img style="width: 80px; height: 80px;"
                                         src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['img'];?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Carregar Nova capa</label>
                                <div class="controls">
                                    <input type="file" name="capa"/>
                                    <input type="hidden" name="capa_atual" value="<?php echo $depoimento['img']; ?>">
                                </div>
                            </div>




                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"><span class="icon"> <i
                                                    class="icon-align-justify"></i> </span>
                                        <h5>Texto do Banner</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">
                                            <textarea class="tinymce span12" rows="40"
                                                      name="texto_titulo"><?php echo $depoimento['texto']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone1']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone1" value="<?php echo $depoimento['titulo_icone1']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao1" value="<?php echo $depoimento['descricao1']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone2']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" name="titulo_icone2" class="span11"
                                           value="<?php echo $depoimento['titulo_icone2']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao2" value="<?php echo $depoimento['descricao2']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone3']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone3" value="<?php echo $depoimento['titulo_icone3']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao3" value="<?php echo $depoimento['descricao3']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone4']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone4" value="<?php echo $depoimento['titulo_icone4']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao4" value="<?php echo $depoimento['descricao4']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone5']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone5" value="<?php echo $depoimento['titulo_icone5']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao5" value="<?php echo $depoimento['descricao5']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone6']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone6" value="<?php echo $depoimento['titulo_icone6']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao6" value="<?php echo $depoimento['descricao6']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone7']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone7" value="<?php echo $depoimento['titulo_icone7']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao7" value="<?php echo $depoimento['descricao7']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone8']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone8" value="<?php echo $depoimento['titulo_icone8']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao8" value="<?php echo $depoimento['descricao8']; ?>">
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                <input type="hidden" name="nome_tabela" value="tb_site.config">
                                <input type="submit" class="btn btn-success" name="acao" value="Atualizar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-Footer-part-->
    <script src="js/jquery.min.js"></script>

    <script src="js/select2.min.js"></script>
    <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

