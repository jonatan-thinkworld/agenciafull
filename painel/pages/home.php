<?php
//Lista usuarios online
$usuariosOnline = Painel::listarUsuariosOnline();

//lista Visitas totais
$pegarVisitasTotais = Painel::totalVisitas();

//Lista visitas Hoje
$pegarVisitasHoje = Painel::visitasHoje();
?>
<div class="box-content left w100">
    <h2><i class="fa fa-home"></i>Painel de Controle - <?php echo NOME_EMPRESA ?></h2>

    <div class="box-metricas">
        <div class="box-metrica-single">
            <div class="box-metrica-wraper">
                <h2>Usuários Online</h2>
                <p><?php echo count($usuariosOnline); ?></p>
            </div><!--BOX-METRICA-WRAPER-->
        </div><!--BOX-METRICA-SINGLE-->
        <div class="box-metrica-single">
            <div class="box-metrica-wraper">
                <h2>Total de Visitas</h2>
                <p><?php echo $pegarVisitasTotais;?></p>
            </div><!--BOX-METRICA-WRAPER-->
        </div><!--BOX-METRICA-SINGLE-->
        <div class="box-metrica-single">
            <div class="box-metrica-wraper">
                <h2>Visitas Hoje</h2>
                <p><?php echo $pegarVisitasHoje;?></p>
            </div><!--BOX-METRICA-WRAPER-->
        </div><!--BOX-METRICA-SINGLE-->
        <div class="clear"></div>
    </div><!--BOX-METRICA-->

</div><!--BOX-CONTENT-->

<div class="box-content w100">
<h2><i class="fa fa-rocket" aria-hidden="true"></i> Usuários Online no Site</h2>
    <div class="table-responsive">
        <div class="row">
            <div class="col">
                <span>IP</span>
            </div><!--COL-->
            <div class="col">
                <span>Última Ação</span>
            </div><!--COL-->
            <div class="clear"></div>
        </div><!--ROW-->
        <?php
        foreach ($usuariosOnline as $key => $value)
        {
        ?>
        <div class="row">
            <div class="col">
                <span><?php echo $value['ip'] ?></span>
            </div><!--COL-->
            <div class="col">
                <span><?php echo date('d/m/Y H:i:s', strtotime($value['ultima_acao'])); ?></span>
            </div><!--COL-->
            <div class="clear"></div>
        </div><!--ROW-->
        <?php }?>
    </div><!--TABLE-RESPONSIVE-->
</div><!--BOX-CONTENT-->

<div class="box-content w100">
    <h2><i class="fa fa-rocket" aria-hidden="true"></i> Usuários do Painel</h2>
    <div class="table-responsive">
        <div class="row">
            <div class="col">
                <span>Nome</span>
            </div><!--COL-->
            <div class="col">
                <span>Cargo</span>
            </div><!--COL-->
            <div class="clear"></div>
        </div><!--ROW-->
        <?php
        $usuariosPainel = MySql::conectar()->prepare("SELECT * FROM `tb._admin.usuarios`");
        $usuariosPainel->execute();
        $usuariosPainel = $usuariosPainel->fetchAll();
        foreach ($usuariosPainel as $key => $value)
        {
            ?>
            <div class="row">
                <div class="col">
                    <span><?php echo $value['user'] ?></span>
                </div><!--COL-->
                <div class="col">
                    <span><?php echo pegaCargo($value['cargo']);?></span>
                </div><!--COL-->
                <div class="clear"></div>
            </div><!--ROW-->
        <?php }?>
    </div><!--TABLE-RESPONSIVE-->
</div><!--BOX-CONTENT-->
<div class="clear"></div>