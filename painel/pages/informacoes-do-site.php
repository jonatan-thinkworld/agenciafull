<?php
//verifica permissão para entrar na página
verificaPermissaoPagina(2);
$depoimento = Painel::selecionar('tb_admin.seo');

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">SEO</a><a href="<?php INCLUDE_PATH_PAINEL ?>listar-clientes"
                                                     class="tip-bottom">Informações do Site</a></div>
        <h1>SEO</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                $titulo = addslashes($_POST['nome']);
                                $contato = addslashes($_POST['descricao']);
                                $keywords = addslashes($_POST['keywords']);
                                $google = addslashes($_POST['google']);
                                if($titulo == '' || $contato == '') {
                                    Painel::alerta('erro', 'Campos vázios não são permitidos!');
                                    Painel::selecionar('tb_admin.seo');
                                }else{
                                    $arr = ['nome'=>$titulo, 'descricao'=>$contato,'keywords'=>$keywords,'google'=>$google, 'id'=>'1','nome_tabela'=>'tb_admin.seo'];
                                    Painel::atualizar($arr);
                                    Painel::alerta('sucesso', 'Cadastro realizado com sucesso!');
                                    $depoimento = Painel::selecionar('tb_admin.seo');
                                }
                                $depoimento = Painel::selecionar('tb_admin.seo');
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Nome do Site</label>
                                <div class="controls">
                                    <input type="text" name="nome" class="span11" value="<?php echo $depoimento['nome']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Descrição do Site</label>
                                <div class="controls">
                                    <input type="text" name="descricao" class="span11"
                                           value="<?php echo $depoimento['descricao']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Palavras Chaves</label>
                                <div class="controls">
                                    <input type="text" name="keywords" class="span11"
                                           value="<?php echo $depoimento['keywords']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Código Google Analytics</label>
                                <div class="controls">
                                    <textarea class="span11" rows="10" name="google" style="resize: none"> <?php echo $depoimento['google']; ?></textarea>
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Atualizar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

