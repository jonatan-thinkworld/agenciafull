<?php
//verifica permissão para entrar na página
verificaPermissaoPagina(2);

if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    $categoria = Painel::selecionar('tb_site.catprojeto', 'id = ?', array($id));

} else {
    Painel::redirecionar('gerenciar-catprojetos');
    die();
}

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="gerenciar-catprojetos" class="tip-bottom">Gerenciar Projetos</a> <a href="<?php INCLUDE_PATH_PAINEL?>" class="current">Editar Categoria</a></div>
        <h1>Editar Categoria</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post"  class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                $slug = Painel::geradorSlug($_POST['nome']);
                                $arr = array_merge($_POST, array('slug' => $slug));
                                $verificar = MySql::conectar()->prepare("SELECT * FROM `tb_site.catprojeto` WHERE nome = ? AND id!=?");
                                $verificar->execute(array($_POST['nome'], $id));
                                if ($verificar->rowCount() > 0) {
                                    Painel::alerta('erro', 'Categoria Existente!');
                                } else {
                                    if (Painel::atualizar($arr)) {
                                        Painel::alerta('sucesso', 'A categoria foi editada com sucesso!');
                                        $categoria = Painel::selecionar('tb_site.catprojeto', 'id = ?', array($id));
                                    } else {
                                        Painel::alerta('erro', 'Campos vázios não são permitidos!');
                                    }
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Categoria</label>
                                <div class="controls">
                                    <input type="text" name="nome" class="span11" value="<?php echo $categoria['nome']; ?>">
                                </div>
                            </div>

                            <div class="form-actions">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                <input type="hidden" name="nome_tabela" value="tb_site.categoria">
                                <input type="submit" class="btn btn-success" name="acao" value="Atualizar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

