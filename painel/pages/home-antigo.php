<?php
//Lista usuarios online
$usuariosOnline = Painel::listarUsuariosOnline();

//lista Visitas totais
$pegarVisitasTotais = Painel::totalVisitas();

//Lista visitas Hoje
$pegarVisitasHoje = Painel::visitasHoje();
?>
<!--main-container-part-->
<div id="content">
    <!--breadcrumbs-->
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo INCLUDE_PATH ?>inicio" title="Ver Site" class="tip-bottom" target=""><i class="icon-home"></i> Ver site </a></div>
    </div>
    <!--End-breadcrumbs-->

    <!--Action boxes-->
    <div class="container-fluid">
        <div class="quick-actions_homepage">
            <ul class="quick-actions">
                <li class="bg_lb"> <a href="<?php echo INCLUDE_PATH ?>inicio" target="_blank"> <i class="icon-home"></i> <span class="label label-important"></span> Ver o Site </a> </li>
                <li class="bg_lg span3"> <a href="<?php INCLUDE_PATH_PAINEL?>cadastrar-post"> <i class="icon-pencil"></i> Cadastrar Post</a> </li>
                <li class="bg_ls"> <a href="<?php INCLUDE_PATH_PAINEL?>cadastrar-clientes"> <i class="icon-pencil"></i> Cadastrar Projetos</a> </li>
                <li class="bg_lo span3"> <a href="<?php INCLUDE_PATH_PAINEL?>listar-clientes"> <i class="icon-list"></i> Listar Projetos</a> </li>

            </ul>
        </div>
        <!--End-Action boxes-->

        <!--Chart-box-->
        <div class="row-fluid">
            <div class="widget-box">
                <div class="widget-title bg_lg"><span class="icon"><i class="icon-signal"></i></span>
                    <h5>Análise do site</h5>
                </div>
                <div class="widget-content" >
                    <div class="row-fluid">
                        <ul class="site-stats">
                            <li class="bg_lh"><i class="icon-user"></i> <strong><?php echo $pegarVisitasTotais;?></strong> <small>Total de Visitas</small></li>
                            <li class="bg_lh"><i class="icon-globe"></i> <strong><?php echo count($usuariosOnline); ?></strong> <small>Usuários Online </small></li>
                            <li class="bg_lh"><i class="icon-dashboard"></i> <strong><?php echo $pegarVisitasHoje;?></strong> <small>Total de Visitas Hoje</small></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--end-main-container-part-->
