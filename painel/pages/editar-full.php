<?php
//verifica permissão para entrar na página
verificaPermissaoPagina(2);
$depoimento = Painel::selecionar('tb_site.full');

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Site</a> <a href="cadastrar-post" class="current">Editar A Full</a></div>
        <h1>Editar Página</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                $texto_titlo = $_POST['texto_titulo'];
                                $titulo_icone1 = addslashes($_POST['titulo_icone1']);
                                $descricao1 = addslashes($_POST['descricao1']);
                                $titulo_icone2 = addslashes($_POST['titulo_icone2']);
                                $descricao2 = addslashes($_POST['descricao2']);
                                $titulo_icone3 = addslashes($_POST['titulo_icone3']);
                                $descricao3 = addslashes($_POST['descricao3']);
                                $titulo_icone4 = addslashes($_POST['titulo_icone4']);
                                $descricao4 = addslashes($_POST['descricao4']);
                                $historia = $_POST['historia'];
                                $missao = $_POST['missao'];
                                $principio = $_POST['principio'];
                                $bio = $_POST['bio'];
                                $capa = $_FILES['capa'];
                                $foto = $_FILES['minha_foto'];
                                $capa_atual = $_POST['capa_atual'];
                                $foto_atual = $_POST['foto_atual'];
                                if ($texto_titlo == '' || $titulo_icone1 == '' || $descricao1 == '' || $titulo_icone2 == '' || $descricao2 == ''
                                    || $titulo_icone3 == '' || $descricao3 == '' || $titulo_icone4 == '' || $descricao4 == '' ) {
                                    Painel::alerta('erro', 'Campos vázios não são permitidos!');
                                    Painel::selecionar('tb_site.full');
                                } else {
                                    if ($capa['name'] != '' && $foto['name'] != '') {
                                        Painel::deleteArquivo($capa_atual);
                                        Painel::deleteArquivo($foto_atual);
                                        $capa = Painel::uploadArquivo($capa);
                                        $foto = Painel::uploadArquivo($foto);
                                        $arr = ['texto' => $texto_titlo, 'capa' => $capa, 'titulo1' => $titulo_icone1, 'descricao1' => $descricao1, 'titulo2' => $titulo_icone2, 'descricao2' => $descricao2, 'titulo3' => $titulo_icone3, 'descricao3' => $descricao3, 'titulo4' => $titulo_icone4, 'descricao4' => $descricao4,'historia'=>$historia,'missao'=>$missao,'principio'=>$principio,'minha_foto'=>$foto,'bio'=>$bio, 'id' => '1', 'nome_tabela' => 'tb_site.full'];
                                        Painel::atualizar($arr);
                                        Painel::alerta('sucesso', 'Página atualizada com sucesso!');
                                        $depoimento = Painel::selecionar('tb_site.full');
                                    }elseif($capa['name'] == '' && $foto['name'] != '') {
                                        $capa = $capa_atual;
                                        Painel::deleteArquivo($foto_atual);
                                        $foto = Painel::uploadArquivo($foto);
                                        $arr = ['texto' => $texto_titlo, 'capa' => $capa, 'titulo1' => $titulo_icone1, 'descricao1' => $descricao1, 'titulo2' => $titulo_icone2, 'descricao2' => $descricao2, 'titulo3' => $titulo_icone3, 'descricao3' => $descricao3, 'titulo4' => $titulo_icone4, 'descricao4' => $descricao4,'historia'=>$historia,'missao'=>$missao,'principio'=>$principio,'minha_foto'=>$foto,'bio'=>$bio, 'id' => '1', 'nome_tabela' => 'tb_site.full'];
                                        Painel::atualizar($arr);
                                        Painel::alerta('sucesso', 'Página atualizada com sucesso!');
                                        $depoimento = Painel::selecionar('tb_site.full');
                                    }elseif ($capa['name'] != '' && $foto['name'] == '') {
                                        $foto = $foto_atual;
                                        Painel::deleteArquivo($capa_atual);
                                        $capa = Painel::uploadArquivo($capa);
                                        $arr = ['texto' => $texto_titlo, 'capa' => $capa, 'titulo1' => $titulo_icone1, 'descricao1' => $descricao1, 'titulo2' => $titulo_icone2, 'descricao2' => $descricao2, 'titulo3' => $titulo_icone3, 'descricao3' => $descricao3, 'titulo4' => $titulo_icone4, 'descricao4' => $descricao4,'historia'=>$historia,'missao'=>$missao,'principio'=>$principio,'minha_foto'=>$foto,'bio'=>$bio, 'id' => '1', 'nome_tabela' => 'tb_site.full'];
                                        Painel::atualizar($arr);
                                        Painel::alerta('sucesso', 'Página atualizada com sucesso!');
                                        $depoimento = Painel::selecionar('tb_site.full');
                                    }elseif ($capa['name']== '' && $foto['name'] == '') {
                                        $capa = $capa_atual;
                                        $foto = $foto_atual;
                                        $arr = ['texto' => $texto_titlo, 'capa' => $capa, 'titulo1' => $titulo_icone1, 'descricao1' => $descricao1, 'titulo2' => $titulo_icone2, 'descricao2' => $descricao2, 'titulo3' => $titulo_icone3, 'descricao3' => $descricao3, 'titulo4' => $titulo_icone4, 'descricao4' => $descricao4,'historia'=>$historia,'missao'=>$missao,'principio'=>$principio,'minha_foto'=>$foto,'bio'=>$bio, 'id' => '1', 'nome_tabela' => 'tb_site.full'];
                                        Painel::atualizar($arr);
                                        Painel::alerta('sucesso', 'Página atualizada com sucesso!');
                                        $depoimento = Painel::selecionar('tb_site.full');
                                    }
                                }
                            }
                            ?>

                            <div class="control-group">
                                <label class="control-label">Capa Antiga</label>
                                <div class="controls">
                                    <img style="width: 80px; height: 80px;"
                                         src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['capa'];?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Carregar Nova capa</label>
                                <div class="controls">
                                    <input type="file" name="capa"/>
                                    <input type="hidden" name="capa_atual" value="<?php echo $depoimento['capa']; ?>">
                                </div>
                            </div>




                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"><span class="icon"> <i
                                                class="icon-align-justify"></i> </span>
                                        <h5>Texto do Banner</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">
                                            <textarea class="tinymce span12" rows="25"
                                                      name="texto_titulo"><?php echo $depoimento['texto']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone1']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone1" value="<?php echo $depoimento['titulo1']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao1" value="<?php echo $depoimento['descricao1']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone2']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" name="titulo_icone2" class="span11"
                                           value="<?php echo $depoimento['titulo2']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao2" value="<?php echo $depoimento['descricao2']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <i class="far fa-star"></i>
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone3']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                    <div class="controls">
                                        <input type="text" class="span11"
                                               name="titulo_icone3" value="<?php echo $depoimento['titulo3']; ?>">
                                    </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao3" value="<?php echo $depoimento['descricao3']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <img style="width: 80px; height: 80px;"
                                     src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['icone4']; ?>">
                            </div>


                            <div class="control-group">
                                <label class="control-label">Titulo</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="titulo_icone4" value="<?php echo $depoimento['titulo4']; ?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Descrição</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="descricao4" value="<?php echo $depoimento['descricao4']; ?>">
                                </div>
                                <hr>
                            </div>


                            <div class="control-group">
                                <label class="control-label">História</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="historia" value="<?php echo $depoimento['historia']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Missão</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="missao" value="<?php echo $depoimento['missao']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Princípio</label>
                                <div class="controls">
                                    <input type="text" class="span11"
                                           name="principio" value="<?php echo $depoimento['principio']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Foto Antiga</label>
                                <div class="controls">
                                    <img style="width: 80px; height: 80px;"
                                         src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $depoimento['minha_foto'];?>">
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Carregar Nova Foto</label>
                                <div class="controls">
                                    <input type="file" name="minha_foto"/>
                                    <input type="hidden" name="foto_atual" value="<?php echo $depoimento['minha_foto']; ?>">
                                </div>
                            </div>

                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"><span class="icon"> <i
                                                    class="icon-align-justify"></i> </span>
                                        <h5>Sobre a Full</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">
                                            <textarea class="tinymce span12" rows="25"
                                                      name="bio"><?php echo $depoimento['bio']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                <input type="hidden" name="nome_tabela" value="tb_site.full">
                                <input type="submit" class="btn btn-success" name="acao" value="Atualizar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end-Footer-part-->
    <script src="js/jquery.min.js"></script>

    <script src="js/select2.min.js"></script>
    <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

