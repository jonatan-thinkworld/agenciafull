<?php
if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    $galeria = Painel::selecionar('tb_admin_imagem', 'id = ?', array($id));
    $b = $galeria['id_produto'];
    $slide = Painel::selecionar('tb_site.galeria', 'id = ?', array($b));

} else {
    Painel::alerta('erro', 'Você precisa passar o parametro ID!');
    die();
}
?>
<style>
    input, textarea, .uneditable-input {
        margin-left: 300px;
        margin-right: 300px;}
</style>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href=    "listar-clientes" class="tip-bottom">Listar Projetos</a> <a href="cadastrar-post" class="current">Editar
                Projeto</a></div>
        <h1>Editar Imagem</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Personal-info</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php
                            if (isset($_POST['acao'])) {
                                //enviei o meu formulário
                                //Painel::alerta('sucesso','Cadastro realizado com sucesso!');
                                $imagem = $_FILES['capa'];
                                $imagem_atual = $_POST['imagem_atual'];
                                $id_produto = $galeria['id_produto'];
                                if ($imagem['name'] != '') {
                                    //EXISTE O UPLOAD DE IMAGEM
                                    if (Painel::imagemValida($imagem)) {
                                        Painel::deleteArquivo($imagem_atual);
                                        $imagem = Painel::uploadArquivo($imagem);
                                        $arr = [ 'id_produto' => $id_produto, 'img' => $imagem, 'order_id' => '0', 'id' => $id, 'nome_tabela' => 'tb_admin_imagem'];
                                        Painel::atualizar($arr);
                                        $slide = Painel::selecionar('tb_admin_imagem', 'id = ?', array($id));
                                        Painel::redirecionar('editar-imagem?id='.$id);
                                        Painel::alerta('sucesso', 'Os dados foram atualizados junto com a imagem');

                                    } else {
                                        Painel::redirecionar('editar-imagem?id='.$id);
                                        Painel::alerta('erro', 'O formato da imagem não é válido!');
                                    }
                                } else {
                                    $imagem = $imagem_atual;
                                    $slug = Painel::geradorSlug($nome);
                                    $arr = [ 'id_produto' => $id_produto, 'img' => $imagem, 'order_id' => '0', 'id' => $id, 'nome_tabela' => 'tb_admin_imagem'];
                                    Painel::atualizar($arr);
                                    $slide = Painel::selecionar('tb_admin_imagem', 'id = ?', array($id));
                                    Painel::redirecionar('editar-imagem?id='.$id);
                                    Painel::alerta('sucesso', 'Os dados foram atualizados com a imagem antiga');
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Capa Atual</label>
                                <div class="controls">
                                    <img style="width: 200px; height: 150px;"
                                         src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $galeria['img']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Nova imagem</label>
                                <div class="controls">
                                    <input type="file" name="capa"/>
                                    <input type="hidden" name="imagem_atual" value="<?php echo $post['capa']; ?>">

                                </div>
                            </div>


                            <div class="form-actions">
                                <a class="btn btn-info"
                                   href="<?php echo INCLUDE_PATH_PAINEL ?>editar-cliente?id=<?php echo $slide['id']?>"><i class="fa fa-times"><< Voltar</i></a>
                                <input type="submit" class="btn btn-success btn-large" name="acao" value="Atualizar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

