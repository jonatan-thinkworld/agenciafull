<?php
if(isset($_GET['excluir'])){
    $idExcluir = intval($_GET['excluir']);
    $selecionarImagem = MySql::conectar()->prepare("SELECT capa FROM `tb_site.blog` WHERE id = ?");
    $selecionarImagem->execute(array($_GET['excluir']));

    $imagem = $selecionarImagem->fetch()['capa'];
    Painel::deleteArquivo($imagem);
    Painel::deletar('tb_site.blog',$idExcluir);
    Painel::redirecionar(INCLUDE_PATH_PAINEL.'gerenciar-post');
}elseif (isset($_GET['order']) && isset($_GET['id'])){
    Painel::ordernarItem('tb_site.blog',$_GET['order'],$_GET['id']);
}
$paginaAtual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
$porPagina = 10;
$post = Painel::selecionarTudo('tb_site.blog',($paginaAtual - 1)* $porPagina,$porPagina);

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php INCLUDE_PATH_PAINEL ?>/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Inicio</a> <a href="#">Blog</a> <a href="#" class="current">Gerenciar Todos os Posts</a> </div>
        <h1>Gerenciar Todos os Posts</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
                        <h5>Listar todas as páginas</h5>
                        <a class="btn btn-success" href="<?php INCLUDE_PATH_PAINEL?>cadastrar-post" style="float: right; margin-top:3px; margin-bottom: 2px; margin-right: 3px"><i class=""></i>Cadastrar Post </a>

                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Categoria</th>
                                <th>Imagem</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <?php
                            foreach ($post as $key => $value) {
                            $nomeCategoria = Painel::selecionar('tb_site.categoria','id=?',array($value['categoria_id']))['nome']   ;

                            ?>
                            <tbody>
                            <tr class="odd gradeX">
                                <td><?php echo $value['titulo']; ?></td>
                                <td><?php echo $nomeCategoria; ?></td>
                                <td><img style="width: 50px; height: 50px;" src="<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $value['capa'];?>"></td>
                                <td><a class="btn btn-warning" href="<?php echo INCLUDE_PATH_PAINEL ?>editar-post?id=<?php echo $value['id'];?>"><i class="fa fa-pencil"></i> Editar</a></td>
                                <td><a actionBtn="delete" class="btn btn-danger" href="<?php echo INCLUDE_PATH_PAINEL ?>gerenciar-post?excluir=<?php echo $value['id']; ?>"><i class="fa fa-times"></i> Excluir</a></td>
                                <td><a class="btn btn-info" href="<?php INCLUDE_PATH_PAINEL?>gerenciar-post?order=up&id=<?php echo $value['id']?>"><i class="icon-arrow-up"></i> </a></td>
                                <td><a class="btn btn-inverse" href="<?php INCLUDE_PATH_PAINEL?>gerenciar-post?order=down&id=<?php echo $value['id']?>"><i class="icon-arrow-down"></i> </a></td>

                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination alternate">
                    <ul>
                        <?php
                        $totalPaginas= ceil(count(Painel::selecionarTudo('tb_site.blog'))/$porPagina);

                        for ($i=1; $i <= $totalPaginas; $i++){
                            if($i == $paginaAtual)
                                echo '<li><a class="page-selected" href="'.INCLUDE_PATH_PAINEL.'gerenciar-post?pagina='.$i.'">'.$i.'</a></li>';
                            else
                                echo '<li><a href="'.INCLUDE_PATH_PAINEL.'gerenciar-post?pagina='.$i.'">'.$i.'</a></li>';

                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div></div>
</div>

