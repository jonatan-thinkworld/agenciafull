<?php
verificaPermissaoPagina(2)
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Projeto</a> <a href="<?php INCLUDE_PATH_PAINEL?>" class="current">Cadastro de Depoimentos</a></div>
        <h1>Cadastrar Depoimento</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post"  class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                $titulo = addslashes( $_POST['titulo']);
                                $conteudo = $_POST['texto'];
                                if($titulo != '' && $conteudo != ''){
                                    $arr = ['titulo' => $titulo, 'texto' => $conteudo,'order_id'=>'0','nome_tabela' => 'tb_site.texto'];
                                    Painel::inserir($arr);
                                    Painel::alerta('sucesso','O cadastro do depoimento foi realizado com suscesso!');
                                }else{
                                    Painel::alerta('erro','campos vazios não são permitidos!');
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Nome do Cliente:</label>
                                <div class="controls">
                                    <input type="text" name="titulo" class="span11" required>
                                </div>
                            </div>



                            <div class="row-fluid">
                                <div class="widget-box">
                                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                                        <h5>Conteúdo:</h5>
                                    </div>
                                    <div class="widget-content">
                                        <div class="control-group">
                                            <textarea class="tinymce span12" rows="15" name="texto"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="hidden" name="order_id" value="">
                                <input type="hidden" name="nome_tabela" value="tb_site.texto">
                                <input type="submit" class="btn btn-success" name="acao" value="Cadartrar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

