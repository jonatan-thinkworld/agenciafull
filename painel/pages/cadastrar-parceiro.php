<?php
verificaPermissaoPagina(2)
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="listar-parceiro" class="tip-bottom">Parceiros</a> <a href="<?php INCLUDE_PATH_PAINEL ?>cadastrar-clientes"
                                                           class="current">Cadastrar Parceiros</a></div>
        <h1>Cadastrar Parceiros</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Nossos Parceiros</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php
                            if(isset($_FILES['arquivo'])){
                                if(count($_FILES['arquivo']['tmp_name']) >0){

                                    for ($q=0;$q<count($_FILES['arquivo']['tmp_name']);$q++ ){
                                        $nomedoarquivo = md5($_FILES['arquivo']['name'][$q].time().rand(0,999)).'.jpg';
                                        move_uploaded_file($_FILES['arquivo']['tmp_name'][$q],BASE_DIR_PAINEL.'/uploads/'.$nomedoarquivo);
                                        $a = ['img' => $nomedoarquivo,'order_id'=>'0', 'nome_tabela' => 'parceiros'];
                                        Painel::inserir($a);
                                    }
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Carregar Imagens</label>
                                <div class="controls">
                                    <input type="file"  name="arquivo[]" multiple/>
                                    <input type="hidden" name="galeria" required>
                                </div>
                            </div>

                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Cadastrar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

