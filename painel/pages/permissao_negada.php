<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Sample pages</a> <a href="#" class="current">Error</a> </div>
        <h1>Permissão Negada!</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                        <h5>ERRO</h5>
                    </div>
                    <div class="widget-content">
                        <div class="error_ex">
                            <h1>403</h1>
                            <h3>Você não tem permissão para acessar essa página!</h3>
                            <p>Fale com o administrador para lhe dar acesso</p>
                            <a class="btn btn-warning btn-big"  href="<?php INCLUDE_PATH_PAINEL?>main">Voltar a Home</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>