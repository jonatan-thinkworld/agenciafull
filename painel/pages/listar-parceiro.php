<?php
if(isset($_GET['excluir'])){
    $idExcluir = intval($_GET['excluir']);
    $selecionarImagem = MySql::conectar()->prepare("SELECT img FROM `parceiros` WHERE id = ?");
    $selecionarImagem->execute(array($_GET['excluir']));

    $imagem = $selecionarImagem->fetch()['img'];
    Painel::deleteArquivo($imagem);
    Painel::deletar('parceiros',$idExcluir);
    Painel::redirecionar(INCLUDE_PATH_PAINEL.'listar-parceiro');
}elseif (isset($_GET['order']) && isset($_GET['id'])){
    Painel::ordernarItem('parceiros',$_GET['order'],$_GET['id']);
}
$paginaAtual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
$porPagina = 10;
$clientes = Painel::selecionarTudo('parceiros',($paginaAtual - 1)* $porPagina,$porPagina);

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php INCLUDE_PATH_PAINEL?>main" title="Vá para o início" class="tip-bottom"><i class="icon-home"></i> Inicio</a> <a href="<?php INCLUDE_PATH_PAINEL ?>">Parceiros</a> <a href="<?php INCLUDE_PATH_PAINEL ?>" class="current">Listar Todos os parceiros</a> </div>
        <h1>Listar Todos os Parceiros</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12"><a class="btn btn-success" href="<?php INCLUDE_PATH_PAINEL?>cadastrar-parceiro" style="float: right; margin-bottom: 5px"><i class=""></i>Cadastrar Parceiro </a>

                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
                        <h5>Listar todos os Parceiros</h5>

                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Imagem</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <?php
                            foreach ($clientes as $key => $value) {
                            ?>
                            <tbody>
                            <tr class="odd gradeX">
                                <td><img style="width: 50px; height: 50px;" src="<?php echo INCLUDE_PATH_PAINEL?>uploads/<?php echo $value['img'];?>"></td>
                                <td><a class="btn btn-warning" href="<?php echo INCLUDE_PATH_PAINEL ?>editar-parceiro?id=<?php echo $value['id'];?>"><i class="fa fa-pencil"></i> Editar</a></td>
                                <td><a actionBtn="delete" class="btn btn-danger" href="<?php echo INCLUDE_PATH_PAINEL ?>listar-parceiro?excluir=<?php echo $value['id']; ?>"><i class="fa fa-times"></i> Excluir</a></td>
                                <td><a class="btn btn-info" href="<?php INCLUDE_PATH_PAINEL?>listar-parceiro?order=up&id=<?php echo $value['id']?>"><i class="icon-arrow-up"></i> </a></td>
                                <td><a class="btn btn-inverse" href="<?php INCLUDE_PATH_PAINEL?>listar-parceiro?order=down&id=<?php echo $value['id']?>"><i class="icon-arrow-down"></i> </a></td>

                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination alternate">
                    <ul>
                        <?php
                        $totalPaginas= ceil(count(Painel::selecionarTudo('tb_site.galeria'))/$porPagina);

                        for ($i=1; $i <= $totalPaginas; $i++){
                            if($i == $paginaAtual)
                                echo '<li><a class="page-selected" href="'.INCLUDE_PATH_PAINEL.'listar-parceiro?pagina='.$i.'">'.$i.'</a></li>';
                            else
                                echo '<li><a href="'.INCLUDE_PATH_PAINEL.'listar-parceiro?pagina='.$i.'">'.$i.'</a></li>';

                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div></div>
</div>

