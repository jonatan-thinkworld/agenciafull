<?php
verificaPermissaoPagina(2)
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Projetos e Depoimentos</a> <a href="<?php INCLUDE_PATH_PAINEL?>" class="current">Cadastrar Categoria do Projeto</a></div>
        <h1>Cadastrar Categoria do Projeto</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Personal-info</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php

                            if (isset($_POST['acao'])) {
                                $nome = $_POST['nome'];
                                if ($nome == '') {
                                    Painel::alerta('erro', 'O campo nome não pode ficar vázio!');
                                } else {
                                    //Apenas cadastrar no banco de dados!
                                    $verifica = MySql::conectar()->prepare("SELECT * FROM `tb_site.catprojeto` WHERE nome = ? ");
                                    $verifica->execute(array($_POST['nome']));
                                    if ($verifica->rowCount() == 0) {
                                        $slug = Painel::geradorSlug($nome);
                                        $arr = ['nome' => $nome, 'slug' => $slug, 'order_id' => '0', 'nome_tabela' => 'tb_site.catprojeto'];
                                        Painel::inserir($arr);
                                        Painel::alerta('sucesso', 'O cadastro da categoria foi realizado com sucesso!');
                                    } else {
                                        Painel::alerta('erro', 'já existe uma categoria com esse nome!');
                                    }
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Nome da Categoria</label>
                                <div class="controls">
                                    <input type="text" name="nome" class="span11" required>
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Cadartrar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

