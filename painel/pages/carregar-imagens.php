<?php
if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    $slide = Painel::selecionar('tb_admin_imagem', 'id = ?', array($id));
}

if(isset($_GET['excluir']) && isset($_GET['id_usuario'])){
    $idExcluir = intval($_GET['excluir']);
    $id_usuario = intval($_GET['id_usuario']);
    $selecionarImagem = MySql::conectar()->prepare("SELECT img FROM `tb_admin_imagem` WHERE id = ?");
    $selecionarImagem->execute(array($_GET['excluir']));
    $imagem = $selecionarImagem->fetch()['img'];
    Painel::deleteArquivo($imagem);
    Painel::deletar('tb_admin_imagem',$idExcluir);

    Painel::redirecionar(INCLUDE_PATH_PAINEL. "carregar-imagens?id={$id_usuario}");

}elseif (isset($_GET['order']) && isset($_GET['id'])){
    Painel::ordernarItem('tb_admin_imagem',$_GET['order'],$_GET['id']);
}
$paginaAtual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
$porPagina = 10;
$clientes = Painel::selecionarTudo('tb_admin_imagem',($paginaAtual - 1)* $porPagina,$porPagina);

?>

<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="<?php INCLUDE_PATH_PAINEL?>listar-projetos" class="tip-bottom">Listar Projetos</a> <a href="<?php INCLUDE_PATH_PAINEL ?>cadastrar-clientes"
                                                           class="current">Cadastrar Imagens</a></div>
        <h1>Cadastrar Imagens</h1>

    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Carregar Mais Imagens</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php
                            if (isset($_FILES['arquivo'])) {
                                if (count($_FILES['arquivo']['tmp_name']) > 0) {

                                    for ($q = 0; $q < count($_FILES['arquivo']['tmp_name']); $q++) {
                                        $nomedoarquivo = md5($_FILES['arquivo']['name'][$q] . time() . rand(0, 999)) . '.jpg';
                                        move_uploaded_file($_FILES['arquivo']['tmp_name'][$q], BASE_DIR_PAINEL.'/uploads/' . $nomedoarquivo);
                                        $a = ['id_produto' => $id, 'img' => $nomedoarquivo, 'order_id' => '0', 'nome_tabela' => 'tb_admin_imagem'];
                                        Painel::inserir($a);
                                    }
                                }
                            }
                            ?>

                            <div class="control-group">
                                <label class="control-label">Carregar Imagens</label>
                                <div class="controls">
                                    <input type="file"  name="arquivo[]" multiple/>
                                    <input type="hidden" name="galeria" required>
                                </div>
                            </div>

                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Cadastrar!">
                                <a class="btn btn-info"
                                   href="<?php echo INCLUDE_PATH_PAINEL ?>listar-clientes"><i class="fa fa-times"><< Voltar</i></a>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Galeria</label>

                                <div class="controls">
                                    <?php

                                    {
                                        ?>
                                        <td>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>


                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
                                            <h5>Listar todas as páginas</h5>
                                        </div>
                                        <div class="widget-content nopadding">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <?php
                                                $infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_admin_imagem` WHERE `id_produto` = $id ");
                                                $infoSite->execute();
                                                $infoSite = $infoSite->fetchAll();
                                                foreach ($infoSite as $key => $value) {
                                                ?>
                                                <tbody>
                                                <tr class="odd gradeX">
                                                    <td><img style="width: 80px; height: 80px;" src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $value['img']; ?>"></td>
                                                    <td><a class="btn btn-warning" href="<?php echo INCLUDE_PATH_PAINEL ?>editar-imagem?id=<?php echo $value['id'];?>"><i class="icon-pencil"></i> Editar</a></td>
                                                    <td><a actionBtn="delete" class="btn btn-danger" href="<?php echo INCLUDE_PATH_PAINEL ?>carregar-imagens?id_usuario=<?php echo $id ;?>&excluir=<?php echo $value['id']; ?>"><i class="icon-remove"></i> Excluir</a></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>