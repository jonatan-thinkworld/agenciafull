<?php
verificaPermissaoPagina(2)
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Início</a>
            <a href="#" class="tip-bottom">Configurações</a> <a href="cadastrar-post" class="current">Adicionar Usuário</a></div>
        <h1>Adicionar Usuários</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Personal-info</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" enctype="multipart/form-data" class="form-horizontal">
                            <?php
                            if (isset($_POST['acao'])) {
                                //enviei o meu formulário
                                $login = addslashes($_POST['login']);
                                $nome = addslashes($_POST['nome']);
                                $senha = md5($_POST['password']);
                                $imagem = $_FILES['imagem'];
                                $cargo = $_POST['cargo'];

                                //VALIDAÇÃO DE CADASTRO
                                if ($login == '') {
                                    Painel::alerta('erro', 'O login está vazio!');
                                } elseif ($nome == '') {
                                    Painel::alerta('erro', 'O nome está vazio!');
                                } elseif ($senha == '') {
                                    Painel::alerta('erro', 'A senha está vazio!');
                                } elseif ($cargo == '') {
                                    Painel::alerta('erro', 'O cargo precisa estar selecionado!');
                                } elseif ($imagem['name'] == '') {
                                    Painel::alerta('erro', 'A imagem precisa estar selecionada!');
                                } else {
                                    //PODEMOS CADASTRAR!
                                    if ($cargo >= $_SESSION['cargo']) {
                                        Painel::alerta('erro', 'Você precisa selecionar um cargo menor que o seu!');
                                    } elseif (Painel::imagemValida($imagem) == false) {
                                        Painel::alerta('erro', 'O formato da imagem não está correto!');
                                    } elseif (Usuario::usuarioExiste($login)) {
                                        Painel::alerta('erro', 'O login já existe! ');

                                    } else {
                                        //SÓ CADASTRAR NO BANCO DE DADOS!
                                        $usuario = new Usuario();
                                        $imagem = Painel::uploadArquivo($imagem);
                                        $usuario->cadastrarUsuario($login, $senha, $imagem, $nome, $cargo);
                                        Painel::alerta('sucesso', 'O cadastro do usuario ' . $login . ' foi feito com sucesso!');
                                    }
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Nome</label>
                                <div class="controls">
                                    <input type="text" name="nome" class="span11" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Login</label>
                                <div class="controls">
                                    <input type="text" name="login" class="span11" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Senha</label>
                                <div class="controls">
                                    <input type="password" name="password" class="span11" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Selecione o Cargo</label>
                                <div class="controls">
                                    <select name="cargo">
                                        <?php
                                        foreach (Painel::$cargo as $key => $value){
                                            if($key < $_SESSION['cargo'])
                                                echo '<option value="'.$key.'">'.$value.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Carregar Imagem</label>
                                <div class="controls">
                                    <input type="file" name="imagem"/>
                                    <input type="hidden" name="imagem_atual" required>
                                </div>
                            </div>

                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Cadartrar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

