<?php
if(isset($_GET['excluir'])){
    $idExcluir = intval($_GET['excluir']);
    Painel::deletar('tb_site.texto',$idExcluir);
    Painel::redirecionar(INCLUDE_PATH_PAINEL.'listar-texto');
}elseif (isset($_GET['order']) && isset($_GET['id'])){
    Painel::ordernarItem('tb_site.texto',$_GET['order'],$_GET['id']);
}
$paginaAtual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
$porPagina = 10;
$depoimentos = Painel::selecionarTudo('tb_site.texto',($paginaAtual - 1)* $porPagina,$porPagina);

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Inicio</a> <a href="#" class="current">Post</a> </div>
        <h1>Listar Depoimentos</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
                        <h5>Listar Depoimentos</h5>
                        <a class="btn btn-success" href="<?php INCLUDE_PATH_PAINEL?>cadastrar-texto" style="float: right; margin-top:3px; margin-bottom: 2px; margin-right: 3px"><i class=""></i>Cadastrar Depoimento</a>

                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Texto</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <?php
                            foreach ($depoimentos as $key => $value) {
                            ?>
                            <tbody>
                            <tr class="odd gradeX">
                                <td><?php echo $value['titulo']; ?></td>
                                <td><?php echo $value['texto']; ?></td>
                                <td><a class="btn btn-warning" href="<?php echo INCLUDE_PATH_PAINEL ?>editar-texto?id=<?php echo $value['id'];?>"><i class="fa fa-pencil"></i> Editar</a></td>
                                <td><a actionBtn="delete" class="btn btn-danger" href="<?php echo INCLUDE_PATH_PAINEL ?>listar-texto?excluir=<?php echo $value['id']; ?>"><i class="fa fa-times"></i> Excluir</a></td>
                                <td><a class="btn btn-info" href="<?php INCLUDE_PATH_PAINEL?>listar-texto?order=up&id=<?php echo $value['id']?>"><i class="icon-arrow-up"></i> </a></td>
                                <td><a class="btn btn-inverse" href="<?php INCLUDE_PATH_PAINEL?>listar-texto?order=down&id=<?php echo $value['id']?>"><i class="icon-arrow-down"></i> </a></td>

                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="pagination alternate">
                    <ul>
                        <?php
                        $totalPaginas= ceil(count(Painel::selecionarTudo('tb_site.texto'))/$porPagina);

                        for ($i=1; $i <= $totalPaginas; $i++){
                            if($i == $paginaAtual)
                                echo '<a class="page-selected" href="'.INCLUDE_PATH_PAINEL.'listar-texto?pagina='.$i.'">'.$i.'</a>';
                            else
                                echo '<a href="'.INCLUDE_PATH_PAINEL.'listar-texto?pagina='.$i.'">'.$i.'</a>';

                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div></div>
</div>

