<?php
if (isset($_GET['id'])) {
    $id = (int)$_GET['id'];
    $slide = Painel::selecionar('parceiros', 'id = ?', array($id));

} else {
    Painel::alerta('erro', 'Você precisa passar o parametro ID!');
    die();
}
?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i> Home</a>
            <a href="#" class="tip-bottom">Parceiros</a><a href="<?php INCLUDE_PATH_PAINEL ?>listar-parceiro"
                                                        class="tip-bottom">Listar Parceiros</a><a
                href="<?php INCLUDE_PATH_PAINEL ?>" class="current">Editar Parceiro</a></div>
        <h1>Editar Parceiro</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>informações</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                //enviei o meu formulário
                                //Painel::alerta('sucesso','Cadastro realizado com sucesso!');
                                $imagem = $_FILES['imagem'];
                                $imagem_atual = $_POST['imagem_atual'];

                                if (isset($_POST['destaque'])) {
                                    $destaque = $_POST['destaque'];
                                } else {
                                    $destaque = 'não';
                                }
                                if ($imagem['name'] != '') {
                                    //EXISTE O UPLOAD DE IMAGEM
                                    if (Painel::imagemValida($imagem)) {
                                        Painel::deleteArquivo($imagem_atual);
                                        $imagem = Painel::uploadArquivo($imagem);
                                        $arr = ['img' => $imagem, 'order_id' => '0', 'id' => $id, 'nome_tabela' => 'parceiros'];
                                        Painel::atualizar($arr);
                                        $slide = Painel::selecionar('tb_site.galeria', 'id = ?', array($id));
                                        Painel::alerta('sucesso', 'Os dados foram atualizados junto com a imagem');
                                    } else {
                                        Painel::alerta('erro', 'O formato da imagem não é válido!');
                                    }
                                } else {
                                    $imagem = $imagem_atual;
                                    $slug = Painel::geradorSlug($nome);
                                    $arr = ['img' => $imagem, 'order_id' => '0', 'id' => $id, 'nome_tabela' => 'parceiros'];
                                    Painel::atualizar($arr);
                                    $slide = Painel::selecionar('tb_site.galeria', 'id = ?', array($id));
                                    Painel::alerta('sucesso', 'Os dados foram atualizados com a imagem antiga');
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Carregar Nova Imagem</label>
                                <div class="controls">
                                    <input type="file" name="imagem"/>
                                    <input type="hidden" name="imagem_atual" value="<?php echo $slide['slide']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Imagem Atual</label>
                                <div class="controls">
                                    <td><img style="width: 80px; height: 80px;"
                                             src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $slide['img']; ?>">
                                    </td>
                                </div>
                            </div>

                            <div class="form-actions">
                                <input type="submit" class="btn btn-success" name="acao" value="Atualizar!">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--end-Footer-part-->
        <script src="js/jquery.min.js"></script>

        <script src="js/select2.min.js"></script>
        <script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

