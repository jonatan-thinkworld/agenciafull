<?php
if(isset($_GET['excluir'])){
    $idExcluir = intval($_GET['excluir']);
    $selecionarImagem = MySql::conectar()->prepare("SELECT slide FROM `tb_site.catprojeto` WHERE id = ?");
    $selecionarImagem->execute(array($_GET['excluir']));

    Painel::deletar('tb_site.categoria',$idExcluir);
    Painel::redirecionar(INCLUDE_PATH_PAINEL.'listar-clientes');
}elseif (isset($_GET['order']) && isset($_GET['id'])){
    Painel::ordernarItem('tb_site.categoria',$_GET['order'],$_GET['id']);
}
$paginaAtual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
$porPagina = 10;
$clientes = Painel::selecionarTudo('tb_site.catprojeto',($paginaAtual - 1)* $porPagina,$porPagina);

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php INCLUDE_PATH_PAINEL?>/main" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Inicio</a> <a href="<?php INCLUDE_PATH_PAINEL?>" >Blog</a> <a href="<?php INCLUDE_PATH_PAINEL?>" class="current">Gerencia categoria</a> </div>
        <h1>Listar Categorias</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
                        <h5>Gerenciar Categorias</h5>
                        <a class="btn btn-success" href="<?php INCLUDE_PATH_PAINEL?>cadastrar-categoria-projeto" style="float: right; margin-top:3px; margin-bottom: 2px; margin-right: 3px"><i class=""></i>Cadastrar Categoria </a>

                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Nome:</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <?php
                            foreach ($clientes as $key => $value) {
                            ?>
                            <tbody>
                            <tr class="odd gradeX">
                                <td><?php echo $value['nome']; ?></td>
                                <td><a class="btn btn-warning" href="<?php echo INCLUDE_PATH_PAINEL ?>editar-categoria-projeto?id=<?php echo $value['id'];?>"><i class="fa fa-pencil"></i> Editar</a></td>
                                <td><a actionBtn="delete" class="btn btn-danger" href="<?php echo INCLUDE_PATH_PAINEL ?>gerenciar-catprojetos?excluir=<?php echo $value['id']; ?>"><i class="fa fa-times"></i> Excluir</a></td>
                                <td><a class="btn btn-info" href="<?php INCLUDE_PATH_PAINEL?>gerenciar-catprojetos?order=up&id=<?php echo $value['id']?>"><i class="icon-arrow-up"></i> </a></td>
                                <td><a class="btn btn-inverse" href="<?php INCLUDE_PATH_PAINEL?>gerenciar-catprojetos?order=down&id=<?php echo $value['id']?>"><i class="icon-arrow-down"></i> </a></td>

                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>

                    </div>


                </div>
                <div class="pagination alternate">
                    <ul>
                        <?php
                        $totalPaginas= ceil(count(Painel::selecionarTudo('tb_site.catprojeto'))/$porPagina);

                        for ($i=1; $i <= $totalPaginas; $i++){
                            if($i == $paginaAtual)
                                echo '<li><a class="page-selected" href="'.INCLUDE_PATH_PAINEL.'gerenciar-catprojetos?pagina='.$i.'">'.$i.'</a></li></div>';
                            else
                                echo '<li><a href="'.INCLUDE_PATH_PAINEL.'gerenciar-catprojetos?pagina='.$i.'">'.$i.'</a></li></div>';

                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div></div>
</div>


