<?php

$slide = Painel::selecionar('tb_admin_blog');

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="inicio" title="Vá para o ínicio" class="tip-bottom"><i class="icon-home"></i>
                Início</a>
            <a href="#" class="tip-bottom">Site</a> <a href="<?php INCLUDE_PATH_PAINEL ?>" class="current">Editar
                Blog</a></div>
        <h1>Editar Blog</h1>
    </div>
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-content nopadding">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data">
                            <?php
                            if (isset($_POST['acao'])) {
                                $conteudo = $_POST['titulo'];
                                $capa = $_FILES['capa'];
                                $imagem_capa = $_POST['imagem_capa'];
                                if ($capa['name'] != '') {
                                    //EXISTE O UPLOAD DE IMAGEM
                                    if (Painel::imagemValida($capa)) {
                                        Painel::deleteArquivo($imagem_capa);
                                        $capa = Painel::uploadArquivo($capa);
                                        $arr = ['titulo' => $conteudo, 'capa' => $capa, 'id' => '1', 'nome_tabela' => 'tb_admin_blog'];
                                        Painel::atualizar($arr);
                                        $slide = Painel::selecionar('tb_admin_blog');
                                        Painel::alerta('sucesso', 'Os dados foram atualizados junto com a imagem');
                                    } else {
                                        Painel::alerta('erro', 'O formato da imagem não é válido!');
                                    }
                                } else {
                                    $capa = $imagem_capa;
                                    $arr = ['titulo' => $conteudo, 'capa' => $capa, 'id' => '1', 'nome_tabela' => 'tb_admin_blog'];
                                    Painel::atualizar($arr);
                                    $slide = Painel::selecionar('tb_admin_blog');
                                    Painel::alerta('sucesso', 'Os dados foram atualizados com a imagem antiga');
                                }
                            }
                            ?>
                            <div class="control-group">
                                <label class="control-label">Capa Antiga</label>
                                <div class="controls">

                                    <img style="width: 80px; height: 80px;"
                                         src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $slide['capa']; ?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Carregar Nova capa</label>
                                <div class="controls">
                                    <input type="file" name="capa"/>
                                    <input type="hidden" name="imagem_capa" value="<?php echo $slide['capa']; ?>">
                                </div>
                            </div>
                            <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                                <h5>Conteúdo:</h5>
                            </div>
                            <div class="widget-content">
                                <div class="control-group">
                                    <textarea class="tinymce span12" rows="15"
                                              name="titulo"><?php echo $slide['titulo']; ?></textarea>
                                </div>
                            </div>


                            <div class="control-group">
                                <div class="form-actions">
                                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                                    <input type="hidden" name="nome_tabela" value="contato">
                                    <input type="submit" class="btn btn-success" name="acao" value="Atualizar!">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--end-Footer-part-->
<script src="js/jquery.min.js"></script>

<script src="js/select2.min.js"></script>
<script src="<?php echo INCLUDE_PATH_PAINEL; ?>js/matrix.form_common.js"></script>

