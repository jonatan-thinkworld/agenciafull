<?php
if (isset($_COOKIE['lembrar'])) {
    $user = $_COOKIE['user'];
    $password = $_COOKIE['password'];
    $sql = MySql::conectar()->prepare("SELECT * FROM `tb._admin.usuarios` WHERE user = ? AND password = ?");
    $sql->execute(array($user, $password));
    if ($sql->rowCount() > 0) {
        $info = $sql->fetch();
        $_SESSION['login'] = true;
        $_SESSION['user'] = $user;
        $_SESSION['password'] = $password;
        $_SESSION['cargo'] = $info['cargo'];
        $_SESSION['nome'] = $info['nome'];
        $_SESSION['img'] = $info['img'];
        Painel::redirecionar(INCLUDE_PATH_PAINEL);
    }

}

?>
<!DOCTYPE html>
<html lang="PT-br">

<head>
    <title>FullDesign Login Admin</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/bootstrap-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/matrix-login.css"/>
    <link href="<?php echo INCLUDE_PATH_PAINEL; ?>font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

</head>
<body>
<div id="loginbox">
    <?php
    if (isset($_POST['acao'])) {
        $user = $_POST['user'];
        $password = md5($_POST['password']);
        $sql = MySql::conectar()->prepare("SELECT * FROM `tb._admin.usuarios` WHERE user = ? AND password = ?");
        $sql->execute(array($user, $password));
        if ($sql->rowCount() > 0) {
            $info = $sql->fetch();
            //LOGAMOS COM SUCESSO
            $_SESSION['login'] = true;
            $_SESSION['user'] = $user;
            $_SESSION['password'] = $password;
            $_SESSION['cargo'] = $info['cargo'];
            $_SESSION['nome'] = $info['nome'];
            $_SESSION['img'] = $info['img'];
            if (isset($_POST['lembrar'])) {
                setcookie('lembrar', true, time() + (60 * 60 * 168), '/');
                setcookie('user', $user, time() + (60 * 60 * 168), '/');
                setcookie('password', $password, time() + (60 * 60 * 168), '/');
            }
            Painel::redirecionar(INCLUDE_PATH_PAINEL);
        } else {
            //FALHOU
            echo '<span class="label label-important"><i class="fa fa-times"></i> Usuário ou senha incorreta!</span>';
        }
    }
    ?>

    <form id="loginform" class="form-vertical" method="post">
        <div class="control-group normal_text"><h3><img src="<?php echo INCLUDE_PATH;?>assets/images/logo.png"
                                                        alt="Logo"/></h3></div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lg"><i class="icon-user"> </i></span>
                    <input type="text" name="user"  placeholder="Login"/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_ly"><i class="icon-lock"></i></span>
                    <input type="password" name="password" placeholder="senha"/>

                </div>

            </div>
        </div>
        <div class="form-actions">
            <span class="pull-left"><a href="#" class="flip-link btn btn-info"
                                       id="to-recover"> Esqueceu a senha ?</a></span>
            <span class="pull-right"><input type="submit" name="acao" class="btn btn-success" value="Logar"/></span>
        </div>
    </form>
    <form id="recoverform" action="#" class="form-vertical">
        <p class="normal_text"> Informe seu e-mail de cadastro para receber as instruções </p>

        <div class="controls">
            <div class="main_input_box">
                <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text"
                                                                                      placeholder="Endereço de e-mail"/>
            </div>
        </div>

        <div class="form-actions">
            <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; Voltar para o Login </a></span>
            <span class="pull-right"><a class="btn btn-info"/>Recuperar</a ></span>
        </div>
    </form>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/matrix.login.js"></script>
</body>

</html>
