<?php
    if(isset($_COOKIE['lembrar'])){
        $user = $_COOKIE['user'];
        $password = $_COOKIE['password'];
        $sql = MySql::conectar()->prepare("SELECT * FROM `tb._admin.usuarios` WHERE user = ? AND password = ?");
        $sql->execute(array($user,$password));
        if ($sql->rowCount() > 0){
            $info = $sql->fetch();
            $_SESSION['login'] = true;
            $_SESSION['user'] = $user;
            $_SESSION['password'] = $password;
            $_SESSION['cargo'] = $info['cargo'];
            $_SESSION['nome'] = $info['nome'];
            $_SESSION['img'] = $info['img'];
            Painel::redirecionar(INCLUDE_PATH_PAINEL);
        }

    }

?>
<!DOCTYPE html>
<html>
<head>
    <title>Painel de Controle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH;?>assets/bower_components/fontawesome/css/font-awesome.min.css">
    <link href="<?php echo INCLUDE_PATH_PAINEL?>css/style.css" rel="stylesheet">
</head>
<body>

<div class="box-login">
    <?php
        if(isset($_POST['acao'])){
            $user = $_POST['user'];
            $password = md5($_POST['password']);
            $sql = MySql::conectar()->prepare("SELECT * FROM `tb._admin.usuarios` WHERE user = ? AND password = ?");
            $sql->execute(array($user,$password));
            if ($sql->rowCount() > 0){
                $info = $sql->fetch();
                //LOGAMOS COM SUCESSO
                $_SESSION['login'] = true;
                $_SESSION['user'] = $user;
                $_SESSION['password'] = $password;
                $_SESSION['cargo'] = $info['cargo'];
                $_SESSION['nome'] = $info['nome'];
                $_SESSION['img'] = $info['img'];
                if(isset($_POST['lembrar'])){
                    setcookie('lembrar',true,time()+(60*60*168),'/');
                    setcookie('user',$user,time()+(60*60*168),'/');
                    setcookie('password',$password,time()+(60*60*168),'/');
                }
                Painel::redirecionar(INCLUDE_PATH_PAINEL);
            }else{
                //FALHOU
                echo '<div class="box-erro"><i class="fa fa-times"></i> Usuário ou senha incorreta!</div>';
            }
        }
    ?>
    <h2>Efetue o Login</h2>
    <form method="post">
        <input type="text" name="user" placeholder="login..." required>
        <input type="password" name="password" placeholder="Senha..." required>
        <div class="form-group-login left">
             <input type="submit" name="acao" value="Logar!">
        </div>
        <div class="form-group-login right">
            <label>Lembrar-me</label>
            <input type="checkbox" name="lembrar">
        </div>
        <div class="clear"></div>
    </form>
</div><!--Fechei box login-->


</body>
</html>