-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21-Maio-2018 às 23:41
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agencia_full`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `contato` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `parceiros`
--

CREATE TABLE `parceiros` (
  `id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb._admin.usuarios`
--

CREATE TABLE `tb._admin.usuarios` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `cargo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_admin.online`
--

CREATE TABLE `tb_admin.online` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `ultima_acao` datetime NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_admin.seo`
--

CREATE TABLE `tb_admin.seo` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `keywords` text NOT NULL,
  `google` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_admin.visitas`
--

CREATE TABLE `tb_admin.visitas` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dia` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_admin_blog`
--

CREATE TABLE `tb_admin_blog` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `capa` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_admin_imagem`
--

CREATE TABLE `tb_admin_imagem` (
  `id` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.blog`
--

CREATE TABLE `tb_site.blog` (
  `id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `conteudo` text NOT NULL,
  `capa` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.categoria`
--

CREATE TABLE `tb_site.categoria` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.catprojeto`
--

CREATE TABLE `tb_site.catprojeto` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.comentarios`
--

CREATE TABLE `tb_site.comentarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comentario` text NOT NULL,
  `blog_id` int(11) NOT NULL,
  `data` date NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.config`
--

CREATE TABLE `tb_site.config` (
  `id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `icone1` varchar(255) NOT NULL,
  `titulo_icone1` varchar(255) NOT NULL,
  `descricao1` text NOT NULL,
  `icone2` varchar(255) NOT NULL,
  `titulo_icone2` varchar(255) NOT NULL,
  `descricao2` text NOT NULL,
  `icone3` varchar(255) NOT NULL,
  `titulo_icone3` varchar(255) NOT NULL,
  `descricao3` text NOT NULL,
  `icone4` varchar(255) NOT NULL,
  `titulo_icone4` varchar(255) NOT NULL,
  `descricao4` text NOT NULL,
  `icone5` varchar(255) NOT NULL,
  `titulo_icone5` varchar(255) NOT NULL,
  `descricao5` text NOT NULL,
  `icone6` varchar(255) NOT NULL,
  `titulo_icone6` varchar(255) NOT NULL,
  `descricao6` text NOT NULL,
  `icone7` varchar(255) NOT NULL,
  `titulo_icone7` varchar(255) NOT NULL,
  `descricao7` text NOT NULL,
  `icone8` varchar(255) NOT NULL,
  `titulo_icone8` varchar(255) NOT NULL,
  `descricao8` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.full`
--

CREATE TABLE `tb_site.full` (
  `id` int(11) NOT NULL,
  `capa` varchar(255) NOT NULL,
  `texto` varchar(255) NOT NULL,
  `titulo1` varchar(255) NOT NULL,
  `descricao1` varchar(255) NOT NULL,
  `titulo2` varchar(255) NOT NULL,
  `descricao2` varchar(255) NOT NULL,
  `titulo3` varchar(255) NOT NULL,
  `descricao3` varchar(255) NOT NULL,
  `titulo4` varchar(255) NOT NULL,
  `descricao4` varchar(255) NOT NULL,
  `historia` varchar(255) NOT NULL,
  `missao` varchar(255) NOT NULL,
  `principio` varchar(255) NOT NULL,
  `minha_foto` varchar(255) NOT NULL,
  `bio` varchar(255) NOT NULL,
  `icone1` varchar(255) NOT NULL,
  `icone2` varchar(255) NOT NULL,
  `icone3` varchar(255) NOT NULL,
  `icone4` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.galeria`
--

CREATE TABLE `tb_site.galeria` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `conteudo` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `img` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `order_id` int(11) NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `destaque` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.inicio`
--

CREATE TABLE `tb_site.inicio` (
  `id` int(11) NOT NULL,
  `titulo_topo` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `titulo_icone1` varchar(255) NOT NULL,
  `img1` varchar(255) NOT NULL,
  `descricao1` varchar(255) NOT NULL,
  `titulo_icone2` varchar(255) NOT NULL,
  `img2` varchar(255) NOT NULL,
  `descricao2` varchar(255) NOT NULL,
  `titulo_icone3` varchar(255) NOT NULL,
  `img3` varchar(255) NOT NULL,
  `descricao3` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.projetos`
--

CREATE TABLE `tb_site.projetos` (
  `id` int(11) NOT NULL,
  `capa` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_site.texto`
--

CREATE TABLE `tb_site.texto` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `texto` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parceiros`
--
ALTER TABLE `parceiros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb._admin.usuarios`
--
ALTER TABLE `tb._admin.usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_admin.online`
--
ALTER TABLE `tb_admin.online`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_admin.seo`
--
ALTER TABLE `tb_admin.seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_admin.visitas`
--
ALTER TABLE `tb_admin.visitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_admin_blog`
--
ALTER TABLE `tb_admin_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_admin_imagem`
--
ALTER TABLE `tb_admin_imagem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.blog`
--
ALTER TABLE `tb_site.blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.categoria`
--
ALTER TABLE `tb_site.categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.catprojeto`
--
ALTER TABLE `tb_site.catprojeto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.comentarios`
--
ALTER TABLE `tb_site.comentarios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.config`
--
ALTER TABLE `tb_site.config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.full`
--
ALTER TABLE `tb_site.full`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.galeria`
--
ALTER TABLE `tb_site.galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.inicio`
--
ALTER TABLE `tb_site.inicio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.projetos`
--
ALTER TABLE `tb_site.projetos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_site.texto`
--
ALTER TABLE `tb_site.texto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `parceiros`
--
ALTER TABLE `parceiros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb._admin.usuarios`
--
ALTER TABLE `tb._admin.usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_admin.online`
--
ALTER TABLE `tb_admin.online`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `tb_admin.seo`
--
ALTER TABLE `tb_admin.seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_admin.visitas`
--
ALTER TABLE `tb_admin.visitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `tb_admin_blog`
--
ALTER TABLE `tb_admin_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_admin_imagem`
--
ALTER TABLE `tb_admin_imagem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `tb_site.blog`
--
ALTER TABLE `tb_site.blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_site.categoria`
--
ALTER TABLE `tb_site.categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_site.catprojeto`
--
ALTER TABLE `tb_site.catprojeto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_site.config`
--
ALTER TABLE `tb_site.config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_site.full`
--
ALTER TABLE `tb_site.full`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_site.galeria`
--
ALTER TABLE `tb_site.galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `tb_site.inicio`
--
ALTER TABLE `tb_site.inicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_site.projetos`
--
ALTER TABLE `tb_site.projetos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_site.texto`
--
ALTER TABLE `tb_site.texto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
