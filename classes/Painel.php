<?php

class Painel
{
    //VERIFICA SE ESTÁ LOGADO
    public static function logado()
    {
        return isset($_SESSION['login']) ? true : false;
    }

    //FUNÇÃO LOGOUT
    public static function loggout()
    {
        setcookie('lembrar', 'true', time() - 1, '/');
        session_destroy();
        header('Location: ' . INCLUDE_PATH_PAINEL);
    }

    //VERIFICA SE A PAGINA EXISTE E CARREGA CASO CONTRÁRIO FICA NA PAGINA INDEX
    public static function carregarPagina()
    {
        if (isset($_GET['url'])) {
            $url = explode('/', $_GET['url']);
            if (file_exists('pages/' . $url[0] . '.php')) {
                require 'pages/' . $url[0] . '.php';
            } else {
                //página não existe
                header('Location: ' . INCLUDE_PATH_PAINEL);
            }
        } else {
            require 'pages/home-antigo.php';
        }
    }

    //LISTA TODOS OS USUÁRIOS ONLINE
    public static function listarUsuariosOnline()
    {
        self::limparUsuariosOnline();
        $sql = MySql::conectar()->prepare("SELECT * FROM `tb_admin.online`");
        $sql->execute();
        return $sql->fetchAll();
    }

    //LIMPA USUARIOS ONLINE PELO TEMPO DE 1 MINUTO
    public static function limparUsuariosOnline()
    {
        $date = date('Y-m-d H:i:s');
        $sql = MySql::conectar()->exec("DELETE FROM `tb_admin.online` WHERE ultima_acao < '$date' - INTERVAL 1 MINUTE");
    }

    //FUNÇÃO PARA CONTAR O TOTAL DE VISITAR ATRAVÉS DO COOKIE DE 1 SEMANA
    public static function totalVisitas()
    {
        $pegarVisitasTotais = MySql::conectar()->prepare("SELECT * FROM `tb_admin.visitas`");
        $pegarVisitasTotais->execute();
        return $pegarVisitasTotais->rowCount();
    }

    //FUNÇÃO PARA CONTAR O TOTAL DE VISITAR ATRAVÉS DO COOKIE DE 1 SEMANA
    public static function visitasHoje()
    {
        $pegarVisitasHoje = MySql::conectar()->prepare("SELECT * FROM `tb_admin.visitas` WHERE dia = ?");
        $pegarVisitasHoje->execute(array(date('Y-m-d')));
        return $pegarVisitasHoje->rowCount();
    }

    //FUNÇÃO PARA CRIAR BOX DE ALERTAS
    public static function alerta($tipo, $mensagem)
    {
        if ($tipo == 'sucesso') {


            echo '<div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading">Sucesso!</h4>'.$mensagem . '</div>';
        } else if ($tipo == 'erro') {

            echo '<div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
              <h4 class="alert-heading">Erro!</h4>' . $mensagem . '</div>';
        }
    }

    //FUNÇÃO PARA VALIDAÇÃO DE TIPOS DE IMAGEM E TAMANHOS
    public static function imagemValida($imagem)
    {
        if ($imagem['type'] == 'image/jpeg' ||
            $imagem['type'] == 'image/jpg' ||
            $imagem['type'] == 'image/png') {
                return true;

        } else {
            return false;
        }
    }


    //FUNÇÃO PARA UPLOAD
    public static function uploadArquivo($file)
    {
        $formatoArquivo = explode('.', $file['name']);
        $imagemNome = uniqid() . '.' . $formatoArquivo[count($formatoArquivo) - 1];
        if (move_uploaded_file($file['tmp_name'], BASE_DIR_PAINEL . '/uploads/' . $imagemNome))
            return $imagemNome;
        else
            return false;
    }

    //FUNÇAÕ PARA DELETAR ARQUIVO
    public static function deleteArquivo($file)
    {
        @unlink('uploads/' . $file);
    }

    //função para pegar cargo
    public static $cargo = [
        '0' => 'Normal',
        '1' => 'Sub Administrador',
        '2' => 'Administrador'];

    //converte os caracteres especiais para comuns mantendo url amigavel
    public static function geradorSlug($str)
    {
        $str = mb_strtolower($str);
        $str = preg_replace('/(â|á|ã)/', 'a', $str);
        $str = preg_replace('/(ê|é)/', 'e', $str);
        $str = preg_replace('/(í|Í)/', 'i', $str);
        $str = preg_replace('/(ú)/', 'u', $str);
        $str = preg_replace('/(ó|ô|õ|Ô)/', 'o', $str);
        $str = preg_replace('/(_|\/|!|\?|#)/', '', $str);
        $str = preg_replace('/( )/', '-', $str);
        $str = preg_replace('/ç/', 'c', $str);
        $str = preg_replace('/(-[-]{1,})/', '-', $str);
        $str = preg_replace('/(,)/', '-', $str);
        $str = strtolower($str);
        return $str;
    }

    //FUNÇÃO PARA CADASTRAR CONTEÚDO NO BANCO DE DADOS
    public static function inserir($arr)
    {
        $certo = true;
        $nome_tabela = $arr['nome_tabela'];
        $query = "INSERT INTO `$nome_tabela` VALUES (null";
        foreach ($arr as $key => $value) {
            $nome = $key;
            $valor = $value;
            if ($nome == 'acao' || $nome == 'nome_tabela')
                continue;
            if ($value == '') {
                $certo = false;
                break;
            }
            $query .= ",?";
            $parametros[] = $value;
        }

        $query .= ")";
        if ($certo == true) {
            $sql = MySql::conectar()->prepare($query);
            $sql->execute($parametros);
            $lastId = MySql::conectar()->lastInsertId();
            $sql = MySql::conectar()->prepare("UPDATE `$nome_tabela` SET order_id = ? WHERE id = $lastId");
            $sql->execute(array($lastId));
        }
        return $certo;
    }

    //FUNÇÃO PARA ATUALIZAR CONTEUDO NO BANCO DE DADOS
    public static function atualizar($arr, $single = false)
    {
        $certo = true;
        $first = false;
        $nome_tabela = $arr['nome_tabela'];

        $query = "UPDATE `$nome_tabela` SET ";
        foreach ($arr as $key => $value) {
            $nome = $key;
            $valor = $value;
            if ($nome == 'acao' || $nome == 'nome_tabela' || $nome == 'id')
                continue;
            if ($value == '') {
                $certo = false;
                break;
            }

            if ($first == false) {
                $first = true;
                $query .= "$nome=?";
            } else {
                $query .= ",$nome=?";
            }

            $parametros[] = $value;
        }

        if ($certo == true) {
            if ($single == false) {
                $parametros[] = $arr['id'];
                $sql = MySql::conectar()->prepare($query . ' WHERE id=?');
                $sql->execute($parametros);
            } else {
                $sql = MySql::conectar()->prepare($query);
                $sql->execute($parametros);
            }
        }
        return $certo;
    }

    //FUNÇÃO PARA LISTAR TUDO
    public static function selecionarTudo($tabela, $start = null, $end = null)
    {
        if ($start == null && $end == null)
            $sql = MySql::conectar()->prepare("SELECT * FROM `$tabela` ORDER BY order_id ASC");
        else
            $sql = MySql::conectar()->prepare("SELECT * FROM `$tabela` ORDER BY order_id ASC LIMIT $start,$end");

        $sql->execute();
        return $sql->fetchAll();
    }

    //FUNÇÃO PARA DELETAR POST
    public static function deletar($tabela, $id = false)
    {
        if ($id == false) {
            $sql = MySql::conectar()->prepare("DELETE FROM `$tabela`");
        } else {
            $sql = MySql::conectar()->prepare("DELETE FROM `$tabela` WHERE id = $id");
        }
        $sql->execute();
    }

    //FUNÇÃO PARA REDIRECIONAR
    public static function redirecionar($url)
    {
        echo '<script>location.href="' . $url . '"</script>';
        die();
    }


    //FUNÇÃO PARA SELECIONAR APENAS 1 REGISTRO
    public static function selecionar($table, $query = '', $arr = '')
    {
        if ($query != false) {
            $sql = MySql::conectar()->prepare("SELECT * FROM `$table` WHERE $query");
            $sql->execute($arr);
        } else {
            $sql = MySql::conectar()->prepare("SELECT * FROM `$table`");
            $sql->execute();
        }
        return $sql->fetch();
    }


    public static function ordernarItem($tabela, $orderType, $idItem)
    {
        if ($orderType == 'up') {
            $infoItemAtual = Painel::selecionar($tabela, 'id=?', array($idItem));
            $order_id = $infoItemAtual['order_id'];
            $itemBefore = MySql::conectar()->prepare("SELECT * FROM `$tabela` WHERE order_id < $order_id ORDER BY order_id DESC LIMIT 1");
            $itemBefore->execute();
            if ($itemBefore->rowCount() == 0)
                return;
            $itemBefore = $itemBefore->fetch();
            Painel::atualizar(array('nome_tabela' => $tabela, 'id' => $itemBefore['id'], 'order_id' => $infoItemAtual['order_id']));
            Painel::atualizar(array('nome_tabela' => $tabela, 'id' => $infoItemAtual['id'], 'order_id' => $itemBefore['order_id']));
        } else if ($orderType == 'down') {
            $infoItemAtual = Painel::selecionar($tabela, 'id=?', array($idItem));
            $order_id = $infoItemAtual['order_id'];
            $itemBefore = MySql::conectar()->prepare("SELECT * FROM `$tabela` WHERE order_id > $order_id ORDER BY order_id ASC LIMIT 1");
            $itemBefore->execute();
            if ($itemBefore->rowCount() == 0)
                return;
            $itemBefore = $itemBefore->fetch();
            Painel::atualizar(array('nome_tabela' => $tabela, 'id' => $itemBefore['id'], 'order_id' => $infoItemAtual['order_id']));
            Painel::atualizar(array('nome_tabela' => $tabela, 'id' => $infoItemAtual['id'], 'order_id' => $itemBefore['order_id']));
        }
    }
}
