<?php

session_start();
date_default_timezone_set('America/Sao_Paulo');
$autoload = function ($class){
require 'classes/'.$class.'.php';
};

//Autoload Localhost
spl_autoload_register($autoload);
define('INCLUDE_PATH','http://localhost/agenciafull/');
define('INCLUDE_PATH_PAINEL',INCLUDE_PATH.'painel/');

//
define('BASE_DIR_PAINEL',__DIR__.'/painel');




/*Autoload WEB
spl_autoload_register($autoload);
define('INCLUDE_PATH','http://fullprojetos.com.br/');
define('INCLUDE_PATH_PAINEL',INCLUDE_PATH.'painel/');*/



//CONECTAR AO BANCO DE DADOS! servidor local
define('HOST','localhost');
define('USER','root');
define('PASSWORD','');
define('DATABASE','agencia_full');


/*CONECTAR AO BANCO DE DADOS no servidor web
define('HOST','mysql.hostinger.com.br');
define('USER','u587853893_full');
define('PASSWORD','c0l0r@d0');
define('DATABASE','u587853893_site'); */


//Variaveis para o painel de controle:
define('NOME_EMPRESA','Agência Full');


//FUNÇÕES

//função para pegar cargo
function pegaCargo($indice){
    return Painel::$cargo[$indice];
}





//FUNÇÃO PARA MOSTRAR MENU SELECIONADO
function selecionadoMenu($par){
    $url = explode('/',@$_GET['url'])[0];
    if($url == $par){
        echo 'class="active"';
    }
}

//VERIFICAR PERMISSÃO MENU
function verificaPermissaoMenu($permissao){
    if($_SESSION['cargo'] >= $permissao){
        return;
    }else{
        echo 'style="display:none;';
    }
}

function verificaPermissaoPagina($permissao){
    if($_SESSION['cargo'] >= $permissao){
        return;
    }else{
        require 'painel/pages/permissao_negada.php';
        die();
    }
}

function recoverPost($post){
    if(isset($_POST[$post])){
        echo $_POST[$post];
    }
}