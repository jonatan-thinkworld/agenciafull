<?php require 'config.php'; ?>
<?php Site::updateUsuarioOnline(); ?>
<?php Site::contador(); ?>
<?php
$infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_admin.seo`");
$infoSite->execute();
$infoSite = $infoSite->fetch();
?>
<!DOCTYPE html>

<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>FullDesign</title>
    <meta itemprop="name" content="<?php echo $infoSite['nome']; ?>"/>
    <meta itemprop="description" content="<?php echo $infoSite['descricao']; ?>">
    <meta itemprop="keywords" content="<?php echo $infoSite['keywords']; ?>">


    <link rel="icon" href="assets/images/faviconfull.png">
    <link rel="icon" href="assets/images/faviconfull.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
   <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,600,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet"
          href="<?php echo INCLUDE_PATH; ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet"
          href="<?php echo INCLUDE_PATH; ?>assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet"
          href="<?php echo INCLUDE_PATH; ?>assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH; ?>assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css"
          href="<?php echo INCLUDE_PATH; ?>assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo INCLUDE_PATH; ?>assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH; ?>assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="<?php echo INCLUDE_PATH; ?>assets/css/custom.css">


    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

    <?php $infoSite['google']; ?>
</head>


<body>


<?php
$url = isset($_GET['url']) ? $_GET['url'] : 'inicio';

if (file_exists('pages/' . $url . '.php')) {
    require('pages/' . $url . '.php');
} else {
    //Página 404
    require('pages/404.php');
}
?>

<!--
START: Footer
-->
<footer class="nk-footer">
    <div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-4">
                <h3 class="h5 mb-20">Agência Full</h3>
                <p class="mb-0">&copy; Todos os direitos reservados</p>

                <p>Desenvolvido por: <a href="https://www.fullprojetos.com.br"> Full Design Estratégico</a></p>
                <a href="https://www.iniciativaempreendedora.org.br/?fromLivewire" target="_blank">
                    <img src="<?php echo INCLUDE_PATH ?>assets/images/google-partner.png" alt="Google Partner" title="Google Partner" width="130px">
                </a>
            </div>
            <div class="col-lg-4" style="padding-top: 46px;"><br>
                <a href="https://www.iniciativaempreendedora.org.br/?fromLivewire" target="_blank">
                    <img src="<?php echo INCLUDE_PATH ?>assets/images/shell.png" alt="Selo Shell" width="190px">
                </a>
                <form method="post">
                    <label>Assine nossa newsletter</label><br>
                    <input type="email" placeholder="NewsLetter" name="email">
                    <input type="submit" name="enviar" value="Assinar">
                </form>
            </div>
            <div class="col-lg-2">
                <div class="nk-links-list" style="padding-top: 16px;">
                    <ul><br>
                        <li><a href="<?php echo INCLUDE_PATH; ?>">Início</a></li>
                        <li><a href="<?php echo INCLUDE_PATH; ?>a-full">A Full</a></li>
                        <a href="<?php echo INCLUDE_PATH; ?>o-que-fazemos">O que fazemos</a>
                        <li><a href="<?php echo INCLUDE_PATH; ?>projetos">Projetos</a></li>
                        <li><a href="<?php echo INCLUDE_PATH; ?>blog">Blog</a></li>
                        <li><a href="<?php echo INCLUDE_PATH; ?>contato">Contato</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="nk-links-list" style="padding-top: 16px;">
                    <ul><br>
                        <li><a href="https://www.facebook.com/agenciafulldesign" target="_blank">Facebook</a></li>
                        <li><a href="https://www.instagram.com/agenciafulldesign/" target="_blank">Instagram</a></li>
                        <li><a href="https://www.linkedin.com/company/full-design-estrat%C3%A9gico/" target="_blank">Linkedin</a>
                        </li>
                        <li><a href="https://www.twitter.com/Ag_FullDesign" target="_blank">Twitter</a></li>
                        <li><a href="https://www.youtube.com/channel/UCQ_2UINQIA7M2cQF3t1BH4g?view_as=subscriber"
                               target="_blank">YouTube</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="voltar-ao-topo"><i class="fa fa-arrow-up" style="color: white"></i></a>
</footer>
<!-- END: Footer -->


</div>
<?php
if (isset($_POST['enviar'])) {
    $email = addslashes($_POST['email']);
    if ($email == '') {
        echo '<script>alert("Erro campo do newslleter está vazio!");</script>';
        Painel::redirecionar(INCLUDE_PATH);
        die();

    }
//inserido no banco
    $arr = ['email' => $email, 'order_id' => '0', 'nome_tabela' => 'newsletter'];
    Painel::inserir($arr);
    echo '<script>alert("Obrigado por se inscrever!");</script>';
    Painel::redirecionar(INCLUDE_PATH);
    die();
}
?>

<!-- START: Scripts -->

<!-- GSAP -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

<!-- Bootstrap -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- nK Share -->
<script src="<?php echo INCLUDE_PATH; ?>assets/plugins/nk-share/nk-share.js"></script>

<!-- Sticky Kit -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

<!-- Jarallax -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/jarallax/dist/jarallax.min.js"></script>
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

<!-- Flickity -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

<!-- Isotope -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

<!-- Photoswipe -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

<!-- Jquery Form -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

<!-- Jquery Validation -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

<!-- Hammer.js -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/hammer.js/hammer.min.js"></script>

<!-- Social Likes -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/social-likes/dist/social-likes.min.js"></script>

<!-- NanoSroller -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

<!-- Keymaster -->
<script src="<?php echo INCLUDE_PATH; ?>assets/bower_components/keymaster/keymaster.js"></script>

<!-- Piroll -->
<script src="<?php echo INCLUDE_PATH; ?>assets/js/piroll.min.js"></script>
<script src="<?php echo INCLUDE_PATH; ?>assets/js/piroll-init.js"></script>

<!-- Demo -->
<script src="<?php echo INCLUDE_PATH; ?>assets/js/demo.js"></script>
<!-- END: Scripts -->


</body>

</html>